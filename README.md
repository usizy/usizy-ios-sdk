# Usizy Ios SDK

[![Version](https://img.shields.io/cocoapods/v/UsizyIosSdk.svg?style=flat)](https://cocoapods.org/pods/UsizyIosSdk)
[![License](https://img.shields.io/cocoapods/l/UsizyIosSdk.svg?style=flat)](https://cocoapods.org/pods/UsizyIosSdk)
[![Platform](https://img.shields.io/cocoapods/p/UsizyIosSdk.svg?style=flat)](https://cocoapods.org/pods/UsizyIosSdk)

## App identifier

In order to enable your app integration you must provide us with your **APP identifier**, you can check it in the following link <https://developer.apple.com/help/account/manage-identifiers/register-an-app-id>

## Requirements

iOS 13.4+
Xcode 12+
Swift 5

## Installation

UsizyIosSdk is available through [CocoaPods](https://cocoapods.org). For more information about using CocoaPods with your app, see the [CocoaPods Getting Started Guide](https://guides.cocoapods.org/using/getting-started.html). Make sure you have the CocoaPods gem installed on your machine before attempting to install any  pods. 

To install it, simply add the following line to your Podfile:

```ruby
pod 'UsizyIosSdk', :tag => '0.11.1'
```

Navigate to your project folder in a terminal window, and run the following command:

```sell
pod install
```


## Usage example 


Add UsizyButton in the IB and configure it
```swift
import UIKit
import UsizyIosSdk

@IBOutlet weak var btUsizy: USizyButton!

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let config = UsizyButtonConfiguration()
        config.productId = "<PRODUCT_ID>"
        btUsizy.initialize(config)
    }

}
```

Or add UsizyButton in the layout and configure it

```swift
import UIKit
import UsizyIosSdk

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btUsizy = USizyButton()
        btUsizy.center = self.view.center
        
        let config = UsizyButtonConfiguration()
        config.productId = "<PRODUCT_ID>"
        btUsizy.initialize(config)
        
        self.view.addSubview(btUsizy)
    }

}
```

Identify the user or use anonymous (Optional)
```swift
config.user = "AnonymousUser"
```

If you want you can customize the logo (Optional)
```swift
config.logoResName = "<XCASSETS_IMAGE_NAME>"
```

If you want you can customize the icon (Optional)
```swift
config.iconResName = "<XCASSETS_IMAGE_NAME>"
```

Add delegate to recover the recommended size (Optional)
```swift
import UIKit
import UsizyIosSdk

class ViewController: UIViewController, UsizyButtonDelegate {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btUsizy = USizyButton()
        btUsizy.center = self.view.center
        
        let config = UsizyButtonConfiguration()
        config.productId = "<PRODUCT_ID>"
        btUsizy.initialize(config)
        
        self.view.addSubview(btUsizy)
    }

    func onRecommendedSize(_ productId: String?, data: SaveRecommendResponse?) {
        // do something
    }
}

```

## Visual Customization of the button

You can customize the icon that appears on the button by specifying the icon parameter in UsizyButtonConfiguration, you can also configure the initial text to be displayed and its color. 
```swift
        let config = UsizyButtonConfiguration()
        config.productId = "<PRODUCT_ID>"
        config.iconResName = "ic_usizy_btn"
        config.title = "What is my size?"
        config.titleColor = UIColor.green
        btUsizy.initialize(config)
```

To set the font and font size we can use the fontFamily attribute
```swift
        let config = UsizyButtonConfiguration()
        config.productId = "<PRODUCT_ID>"
        config.fontFamily = UIFont(name: "Menlo-Regular", size: 40)
        btUsizy.initialize(config)
```


## Sales Confirmation

You can send a sales confirmation with the following code

```swift
import UsizyIosSdk

do {
    let request = try ConfirmRequest(order_id: "USIZY_IOS_SDK_ORDER_ID", product_ids: ["PRODUCT_ID_1"], sizes: ["SIZE_1"])
    UsizyApi.confirm(request) { (errors: ConfirmResponseErrors?) in
        if (errors != nil) {
            debugPrint(errors!.description)
        }
    }
} catch {
    print("Unexpected error!!! \(error)")
}
```

The order_id, product_ids and sizes fields are mandatory.

The fields in ConfirmRequest are:
    var order_id: String
    var product_ids: [String]
    var variation_ids: [String]?
    var sizes: [String]
    var sizes_system: [String]?
    var currency: String?
    var prices_vat: [String]?
    var prices_no_vat: [String]?
    var total_vat: String?
    var total_no_vat: String?
    var shipping_cost: String?


## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Sample Apps

* [uSizy Ios Test](https://bitbucket.org/usizy/usizy-ios-test)

## License

UsizyIosSdk is available under the MIT license. See the LICENSE file for more info.
