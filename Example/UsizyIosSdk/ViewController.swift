//
//  ViewController.swift
//  UsizyIosSdk
//
//  Created by 11569480 on 02/25/2021.
//  Copyright (c) 2021 11569480. All rights reserved.
//

import UIKit
import UsizyIosSdk

class ViewController: UIViewController, UsizyButtonDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var btUsizy: USizyButton!
    
    let productIds = [
        "BOY_SHOES",
        "FEMALE_SHOES",
        "UNISEX_SHOES",
        "MALE_SHOES",
        "HUMAN_SHOES",
        "BABY_UPPER",
        "BOY_FULL",
        "BOY_LOWER",
        "BOY_UPPER",
        "CHILD_FULL",
        "CHILD_UPPER",
        "FEMALE_FULL",
        "FEMALE_LOWER",
        "FEMALE_UPPER",
        "GIRL_FULL",
        "GIRL_LOWER",
        "GIRL_UPPER",
        "HUMAN",
        "MALE_FULL",
        "MALE_LOWER",
        "MALE_UPPER",
        "PANT_WOMAN",
        "UNISEX_UPPER",
        "UNISEX",
        "UNISEX_FULL",
        "UNISEX_LOWER",
        "HUMAN_UPPER"
    ]
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()

        let config = UsizyButtonConfiguration()
        config.productId = "BABY_UPPER"
        config.user = "AnonymousUser"
        btUsizy.delegate = self
        btUsizy.initialize(config)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func onRecommendedSize(_ productId: String?, data: SaveRecommendResponse?) {
        // do something
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productIds.count
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductTableViewCell
        
        cell.lbProduct.text = productIds[indexPath.row]
        cell.selectionStyle = .none
        
        let config = UsizyButtonConfiguration()
        config.productId = productIds[indexPath.row]
        config.user = "AnonymousUser"
        cell.vwUsizy.delegate = self
        cell.vwUsizy.initialize(config)
        
        return cell
    }
}

