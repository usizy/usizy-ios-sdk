//
//  ShoesSizeSelectViewController.swift
//  UsizyIosSdk
//
//  Created by Cloud District on 26/4/21.
//

import UIKit

class ShoesSizeSelectViewController: UIViewController,  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let reuseIdentifier = "ShoeSizeCell"
    private let itemsPerRow: CGFloat = 5
    private let sectionInsets = UIEdgeInsets(
      top: 0.0,
        left: 2.5,
      bottom: 5.0,
        right: 2.5)
    
    // MARK: - Private vars
    private var shoeSizesByRegion: [ShoeSizes]?
    private var selectedShoeSystem: String?
    private var shoeSize: ShoeSizes?
    private var subjectDataMetric: SubjectDataMetric?
    private var subjectDataImperial: SubjectDataImperial?
    
    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    var shoeFav: ShoeFavs?
    
    // MARK: - IBOutlet
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var ivBrand: UIImageView!
    @IBOutlet weak var lbBrandName: UILabel!
    @IBOutlet weak var scRegion: UISegmentedControl!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var cvSizes: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariables()
        initViews()
    }
    
    private func initVariables() {
        let dataMetric: DataMetric = (popupData?.data?.metric!)!
        let dataImperial: DataImperial = (popupData?.data?.imperial!)!
        let sex: String = (popupData?.sex)!
        
        shoeSizesByRegion = [ShoeSizes]()
        selectedShoeSystem = popupData?.shoe_system
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(dataMetric: dataMetric, sex: sex, selectedSex: nil)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(dataImperial: dataImperial, sex: sex, selectedSex: nil)
        shoeSize = ShoeSizes()
        // setear el array de data -> para el segmented como para la tabla
        loadshoeSizesByRegion()
    }
    
    private func loadshoeSizesByRegion() {
        let sizesFromFav = shoeFav?.sizes
        
        for (region, sizes) in sizesFromFav! {
            let shoeSize = ShoeSizes()
            shoeSize.region = region
            var sizesFlatArray = [String]()
            for size in sizes {
                sizesFlatArray.append(size[0].origin!)
            }
            shoeSize.values = sizesFlatArray
            shoeSizesByRegion?.append(shoeSize)
        }
        
        shoeSizesByRegion = shoeSizesByRegion?.sorted(by: {
            $0.region! < $1.region!
        })
    }
    
    private func initViews() {
        loadLabelsStaticText()
        loadBrandInfo()
        loadCollectionView()
        segmentedControlSetup()
    }
    
    private func loadCollectionView() {
        cvSizes.delegate = self
        cvSizes.dataSource = self
    }
    
    private func loadBrandInfo() {
        loadImageView()
        lbBrandName.text = shoeFav?.text
    }
    
    private func loadImageView() {
        let url = shoeFav?.thumb
        
        if !isNilOrBlank(str: url) {
            requestImage(url: url!) { image in
                self.ivBrand.image = image
            }
        }
    }
    
    private func loadLabelsStaticText() {
        lbTitle.text = localizedStringBundle(key: "title.shoesize", for: self.classForCoder)
        btnBack.setTitle(localizedStringBundle(key: "buttons.return", for: self.classForCoder).uppercased(), for: .normal)
    }
    
    private func segmentedControlSetup() {
        scRegion.removeAllSegments()
        
        for index in 0..<shoeSizesByRegion!.count {
            scRegion.insertSegment(withTitle: shoeSizesByRegion![index].region, at: index, animated: false)
            if shoeSizesByRegion![index].region == selectedShoeSystem {
                scRegion.selectedSegmentIndex = index
                shoeSize = shoeSizesByRegion![index]
            }
        }
        scRegion.layer.cornerRadius = 4.0
        scRegion.backgroundColor = UIColor.white
        scRegion.tintColor = UIColor.colorPrimary
        
        if #available(iOS 13.0, *) {
            scRegion.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            scRegion.selectedSegmentTintColor = UIColor.colorPrimary
        }
        
        scRegion.addTarget(self,  action: #selector(changeShoeSystem(sender:)), for: .valueChanged)
    }
    
    // MARK: - #selector
    @objc func changeShoeSystem(sender: UISegmentedControl) {
        
        selectedShoeSystem = shoeSizesByRegion?[sender.selectedSegmentIndex].region
        shoeSize = shoeSizesByRegion?[sender.selectedSegmentIndex]
        cvSizes.reloadData()
    }
    
    @IBAction func onBackPressed(_ sender: UIButton) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let shoeForm = storyboard.instantiateViewController(withIdentifier: "ShoesFormViewController") as! ShoesFormViewController
        shoeForm.contentView = contentView
        shoeForm.popupData = popupData
        shoeForm.product = self.product
        
        contentView!.show(from: self, to: shoeForm)
    }
    
    // MARK: - UICollectionViewDataSource
     func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1
     }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shoeSize?.values?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ShoeSizeCollectionViewCell
        let item = self.shoeSize?.values?[indexPath.row]
        
        cell.lbSize.text = item
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1.0
        
        return cell
    }
    
    func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
      let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
      let availableWidth = view.frame.width - paddingSpace
      let widthPerItem = availableWidth / itemsPerRow
      let heightPerItem = widthPerItem / 2
      return CGSize(width: widthPerItem, height: heightPerItem)
    }
   
    func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      insetForSectionAt section: Int
    ) -> UIEdgeInsets {
      return sectionInsets
    }

    func collectionView(
      _ collectionView: UICollectionView,
      layout collectionViewLayout: UICollectionViewLayout,
      minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
      return sectionInsets.bottom
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let resultsForm = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        
        let size = self.shoeSize?.values?[indexPath.row]
        let finalSize = self.realSize(shoeSize: size!)
        resultsForm.request = getSaveRecommendRequest(shoeSize: finalSize)
        resultsForm.contentView = contentView
        resultsForm.popupData = popupData
        resultsForm.product = self.product
        
        contentView!.show(from: self, to: resultsForm)
    }
    
    func realSize(shoeSize: String) -> String {
        let systemSizes = self.shoeFav?.sizes?[self.selectedShoeSystem!]
        for size in systemSizes! {
            let origin = size[0].origin!
            if (origin == shoeSize) {
                let conversion = size[1].conversion!
                return conversion.last!
            }
        }
        return shoeSize
    }
    
    private func getSaveRecommendRequest(shoeSize: String) -> SaveRecommendRequest {
        let request: SaveRecommendRequest = SaveRecommendRequest(product: self.product)
        
        if (popupData?.metric_system == MetricSystem.METRIC.rawValue) {
            request.age = subjectDataMetric?.age
        } else {
            request.age = subjectDataImperial?.age
        }
        request.ck = popupData?.ck
        request.footType = subjectDataMetric?.foot_type
        request.metric_system = popupData?.metric_system
        request.w = RecommendationWay.BASIC_WAY.rawValue
        request.shoe_brand = shoeFav?.id
        request.shoe_size = shoeSize
        request.sex = popupData?.sex
        
        return request
    }
}
