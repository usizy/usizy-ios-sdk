//
//  SizeCache.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 02/03/2021.
//

import Foundation


class SizeCache {
    
    private var _sid: String? = nil
    private var _sk: String? = nil
    
    var sid: String? {
      set {
        _sid = newValue
        UserDefaults.standard.set(newValue, forKey: "sid")
      }
      get { return _sid }
    }
    
    var sk: String? {
      set {
        _sk = newValue
        UserDefaults.standard.set(newValue, forKey: "sk")
      }
      get { return _sk }
    }
    
    init() {
        _sid = UserDefaults.standard.string(forKey: "sid")
        _sk = UserDefaults.standard.string(forKey: "sk")
    }
    
    static let shared = SizeCache()
    
    var cacheDisplay = [String: String]()
    var cacheRecommend = [String: String]()
    var cacheUser: String? = nil
}
