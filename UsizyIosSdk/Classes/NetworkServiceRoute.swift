//
//  NetworkServiceRoute.swift
//  Pods
//
//  Created by Pedro Aquilino on 30/8/24.
//

import Foundation

enum NetworkServiceRoute: URLRequestProtocol {
    case displayButton(parameters: DisplayButtonRequest)
    case openPopup(parameters: OpenPopupRequest)
    case saveRecommend(parameters: SaveRecommendRequest)
    case shoeBrand(parameters: ShoeBrandRequest)
    case confirm(parameters: ConfirmRequest)
    
    static let encoder = JSONEncoder()
    
    var url: URL {
        switch self {
        case .displayButton:
            return URL(string: "https://usizy.com/api/v1/display-button")!
        case .openPopup:
            return URL(string: "https://usizy.com/api/v1/open-popup")!
        case .saveRecommend:
            return URL(string: "https://usizy.com/api/v1/save-recommend")!
        case .shoeBrand:
            return URL(string: "https://usizy.com/api/v1/shoe-brand")!
        case .confirm:
            return URL(string: "https://usizy.com/api/v2/event/confirm")!
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var body: Data? {
        switch self {
        case .displayButton(let parameters):
            return try? NetworkServiceRoute.encoder.encode(parameters)
        case .openPopup(let parameters):
            return try? NetworkServiceRoute.encoder.encode(parameters)
        case .saveRecommend(let parameters):
            return try? NetworkServiceRoute.encoder.encode(parameters)
        case .shoeBrand(let parameters):
            return try? NetworkServiceRoute.encoder.encode(parameters)
        case .confirm(let parameters):
            return try? NetworkServiceRoute.encoder.encode(parameters)
        }
    }

    var headers: [String: String]? {
        return [
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
    }
}
