//
//  USizyButton.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 26/02/2021.
//

import Foundation
import UIKit

public class USizyButton: UIView {
    
    // MARK: - Public vars
    public var delegate: UsizyButtonDelegate?
    
    // MARK: - Internal vars
    private var productId: String?
    private var user: String?
    private var logoResName: String?
    private var iconResName: String?
    private var title: String?
    private var titleColor: UIColor?
    private var fontFamily: UIFont?
    private var button: UIButton?
    private var image: UIImageView?
    
    // MARK: - LifeCicle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        removeNotificationObserver()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        if (frame.width <= 0){
            self.frame = CGRect(x: 0, y: 0, width: 150, height: 40)
        }
    }
    
    // MARK: - Internal
    func addNotificationObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveRecommendedSize(_:)), name: Notification.Name("didReceiveRecommendedSize"), object: nil)
    }
    
    func removeNotificationObserver(){
        NotificationCenter.default.removeObserver(self, name: Notification.Name("didReceiveRecommendedSize"), object: nil)
    }
    
    func addTouchSelector(){
        self.button?.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
    }
    

    func initializeAppearance(){
        self.button = UIButton(frame: CGRect(x: 30, y: 0, width: self.frame.width-30, height: self.frame.height))
        
        if self.fontFamily != nil {
            self.button?.titleLabel?.font = self.fontFamily
            self.button?.adjustFontSizeToWidth = false
        } else {
            self.button?.adjustFontSizeToWidth = true
        }
        
        if self.titleColor != nil {
            self.button?.setTitleColor(self.titleColor, for: .normal)
            self.button?.setTitleColor(self.titleColor, for: .highlighted)
        } else {
            if #available(iOS 13.0, *) {
                let textColor =  UIColor { tc in
                    switch tc.userInterfaceStyle {
                    case .dark:
                        return UIColor.white
                    default:
                        return UIColor.black
                    }
                }
                
                self.button?.setTitleColor(textColor, for: .normal)
            } else {
                self.button?.setTitleColor(UIColor.colorPrimary, for: .normal)
            }
            self.button?.setTitleColor(UIColor.defaultColor, for: .highlighted)
        }
        
        self.button?.contentHorizontalAlignment = .left
        self.button?.contentVerticalAlignment = .center
        self.button?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        self.addSubview(self.button!)
        
        if let iconResName = self.iconResName {
            let img = UIImage(named: iconResName)
            self.image = UIImageView(image: img)
        } else {
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    let img = loadImageBundle(named: "ic_usizy_btn_dark", for: self.classForCoder)
                    self.image = UIImageView(image: img)
                }
                else {
                    let img = loadImageBundle(named: "ic_usizy_btn", for: self.classForCoder)
                    self.image = UIImageView(image: img)
                }
            }
        }
        
        self.image?.frame = CGRect(x: 0, y: self.frame.height/2 - 12, width: 24, height:24)
        self.image?.contentMode = .scaleAspectFit
        self.addSubview(self.image!)
        
        self.isHidden = true
    }
    
    func localized() {
        if self.title != nil {
            self.button?.setTitle(self.title, for: .normal)
        } else {
            let text = localizedStringBundle(key: "buttons.recommendme", for: self.classForCoder)
            self.button?.setTitle(text, for: .normal)
        }
    }
    
    func fetchDisplayButton(){
        let request = DisplayButtonRequest(product: self.productId)
        SizeQuery().displayButton(request) { (response: DisplayButtonResponse?) in
            if let show = response?.show {
                switch show {
                    case ShowProduct.APP:
                        self.isHidden = false
                    default:
                        self.isHidden = true
                }
                SizeCache.shared.cacheDisplay[self.productId!] = show.rawValue
                
                if (SizeCache.shared.sid == nil && ((response?.sid) != nil)){
                    SizeCache.shared.sid = response?.sid
                }
                if (SizeCache.shared.sk == nil  && ((response?.sk) != nil)){
                    SizeCache.shared.sk = response?.sk
                }
            }else{
                SizeCache.shared.cacheDisplay[self.productId!] = ShowProduct.ERROR.rawValue
            }
        }
    }
    
    func checkVisibility(){
        if (self.productId == nil) {
            self.isHidden = true
            return
        }
        
        let recommend = SizeCache.shared.cacheRecommend[self.productId!]
        if (recommend != nil){
            self.button?.setTitle(recommend!, for: .normal)
            self.isHidden = false
            return
        }

        let display = SizeCache.shared.cacheDisplay[self.productId!]
        if (display != nil && display! == ShowProduct.APP.rawValue){
            self.isHidden = false
            return
        }else if (display != nil){
            self.isHidden = true
            return
        }
        
        fetchDisplayButton()
    }
    
    // MARK: - Selectors
    @objc func buttonClicked() {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
        vc.logoResName = self.logoResName
        vc.product = self.productId
        
        vc.modalPresentationStyle = .fullScreen
        
        let rootVc = UIApplication.shared.keyWindow?.rootViewController
        rootVc?.present(vc, animated: false, completion: nil)
    }
    
    @objc func onDidReceiveRecommendedSize(_ notification: Notification) {
        var data:SaveRecommendResponse? = nil
        if let dict = notification.userInfo as NSDictionary? {
            if let dataValid = dict["response"] as? SaveRecommendResponse, let id = dict["product"] as? String{
                if (id == self.productId){
                    data = dataValid
                    
                    self.button?.setTitle(dataValid.usizy_size, for: .normal)
                    SizeCache.shared.cacheRecommend[self.productId!] = dataValid.usizy_size
                    self.delegate?.onRecommendedSize(self.productId, data: data)
                }
            }
        }
    }

    // MARK: - Public
    public func initialize(_ configuration: UsizyButtonConfiguration){
        self.productId = configuration.productId
        self.user = configuration.user
        self.logoResName = configuration.logoResName
        self.iconResName = configuration.iconResName
        self.title = configuration.title
        self.titleColor = configuration.titleColor
        self.fontFamily = configuration.fontFamily
        
        initializeAppearance()
        addTouchSelector()
        addNotificationObserver()
        localized()
    
        SizeCache.shared.cacheUser = self.user
        
        checkVisibility()
    }
}

extension UIButton {
    @IBInspectable var adjustFontSizeToWidth: Bool {
            get {
                return ((self.titleLabel?.adjustsFontSizeToFitWidth) != nil)
            }
            set {
                self.titleLabel?.numberOfLines = 1
                self.titleLabel?.adjustsFontSizeToFitWidth = newValue;
                self.titleLabel?.lineBreakMode = .byClipping;
                self.titleLabel?.baselineAdjustment = .alignCenters
            }
        }
}
