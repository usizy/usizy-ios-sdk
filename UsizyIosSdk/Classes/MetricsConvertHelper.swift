//
//  MetricsConvertHelper.swift
//  Pods
//
//  Created by David Arcis on 5/3/21.
//

import Foundation

class MetricsConvertHelpers {
    
    static func fromFeetInchesToCm (feet: Float, inches: Float) -> Float {
        let totalInches: Float = feet * 12 + inches
        return totalInches * SystemConversionFactors.CONVERSION_FACTOR_A.rawValue
    }
    
    static func getTotalInchesFromCm(cm: Float) -> Float {
        return cm / SystemConversionFactors.CONVERSION_FACTOR_A.rawValue
    }
    
    static func fromInchesToCm(inches: Float) -> Float {
        return inches * SystemConversionFactors.CONVERSION_FACTOR_A.rawValue
    }
    
    static func getTotalInchesFromImperial(heightImperial: HeightImperial) -> Float {
        return heightImperial.ft! * SystemConversionFactors.CONVERSION_FACTOR_B.rawValue + heightImperial.inX!
    }

    static func getImperialFromTotalInches (totalInches: Float) -> HeightImperial {
        let feet = floor(totalInches / SystemConversionFactors.CONVERSION_FACTOR_B.rawValue)
        let inX =  totalInches.truncatingRemainder(dividingBy: SystemConversionFactors.CONVERSION_FACTOR_B.rawValue)
        return HeightImperial(ft: feet, inX: inX)
    }

    static func fromKgToPounds(kgs: Float) -> Float {
        return kgs / SystemConversionFactors.CONVERSION_WEIGHT_FACTOR.rawValue
    }

    static func fromPoundsToKgs(pounds: Float) -> Float {
        return pounds * SystemConversionFactors.CONVERSION_WEIGHT_FACTOR.rawValue
    }
    
    static func getDecimalPart(num: Float) -> Float {
        return abs(num - Float(Int(floor(num))))
    }
}
