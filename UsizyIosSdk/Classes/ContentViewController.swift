//
//  ContentViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 01/03/2021.
//

import UIKit

class ContentViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var btPrivacy: UIButton!
    @IBOutlet weak var ivLogo: UIImageView!
    
    // MARK: - Public vars
    var logoResName: String?
    var product: String?
    var logoUrl: String?
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedBetweenFormsData.shared.clearValues()
        localized()
        fetchPopupData()
        initializeAppearance()
        
        
    }
    
    // MARK: - Internal
    private func initializeAppearance(){
        setUpBrandLogo()
    }
    
    private func setUpBrandLogo() {
        if !isNilOrBlank(str: logoUrl) {
            requestImage(url: logoUrl! ) { image in
                self.ivLogo.image = image
            }
        } else if let imageLogo = self.logoResName{
            ivLogo.image = UIImage(named: imageLogo)
        }
    }
    
    func localized(){
        btPrivacy.setTitle(localizedStringBundle(key: "buttons.privacy", for: self.classForCoder), for: .normal)
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = CGRect.init(x: 0, y: 128, width: view.bounds.width, height: view.bounds.height-128)
        // viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }

    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func fetchPopupData(){

        if (self.product == nil){
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let loader = storyboard.instantiateViewController(withIdentifier: "LoaderViewController")
        add(asChildViewController: loader)
        
        let request = OpenPopupRequest(product: self.product!)
        SizeQuery().openPopup(request) { (response: OpenPopupResponse?) in
            if (response != nil){
                self.logoUrl = response?.logo
                
                if (response?.zone == PopupZones.SHOES.rawValue){
                    let mainForm = storyboard.instantiateViewController(withIdentifier: "ShoesFormViewController") as! ShoesFormViewController
                    mainForm.contentView = self
                    mainForm.popupData = response
                    mainForm.product = self.product
                    self.show(from: loader, to: mainForm)
                }else{
                    let mainForm = storyboard.instantiateViewController(withIdentifier: "MainFormViewController") as! MainFormViewController
                    mainForm.contentView = self
                    mainForm.popupData = response
                    mainForm.product = self.product
                    self.show(from: loader, to: mainForm)
                }
            }else{
                let errorForm = storyboard.instantiateViewController(withIdentifier: "ErrorViewController") as! ErrorViewController
                errorForm.contentView = self
                errorForm.popupData = response
                errorForm.product = self.product
                self.show(from: loader, to: errorForm)
            }
        }
    }
    
    // MARK: - IBAction
    @IBAction func showPrivacy(_ sender: UIButton) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let vc = storyboard.instantiateViewController(withIdentifier: "PrivacyViewController")
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Public
    public func show(from vc1: UIViewController, to vc2: UIViewController){
        remove(asChildViewController: vc1)
        add(asChildViewController: vc2)
    }
}

