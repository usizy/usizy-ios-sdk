//
//  ShoesImageSelectorViewController.swift
//  UsizyIosSdk
//
//  Created by Daniel Almería Núñez on 22/4/21.
//

import UIKit

class ShoesImageSelectorViewController: UIViewController {

    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    var request: SaveRecommendRequest?
    var footType: Int = 1
    
    // MARK: - Life Cicle
    @IBOutlet weak var lbFootTitle: UILabel!
    @IBOutlet weak var lbFootInfo: UILabel!
    @IBOutlet weak var btContinue: UIButton!
    @IBOutlet weak var btBack: UIButton!
    
    // Foot
    @IBOutlet weak var iv1Foot: UIImageView!
    @IBOutlet weak var lb1Foot: UILabel!
    @IBOutlet weak var check1Foot: UIImageView!
    @IBOutlet weak var iv2Foot: UIImageView!
    @IBOutlet weak var lb2Foot: UILabel!
    @IBOutlet weak var check2Foot: UIImageView!
    
    // MARK: - Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        
        updateFootViews()
    }

    // MARK: - Internal
    private func initViews() {
        btBack.setTitle(localizedStringBundle(key: "buttons.return", for: self.classForCoder).uppercased(), for: .normal)
        btContinue.setTitle(localizedStringBundle(key: "buttons.continue", for: self.classForCoder).uppercased(), for: .normal)
        lbFootTitle.text = localizedStringBundle(key: "title.foot", for: self.classForCoder)
        lbFootInfo.text = localizedStringBundle(key: "description.foot", for: self.classForCoder)
    }
    
    private func updateFootViews(){
        lb1Foot.font = UIFont.systemFont(ofSize: 17.0)
        lb2Foot.font = UIFont.systemFont(ofSize: 17.0)
        check1Foot.isHidden = true
        check2Foot.isHidden = true
        
        if (footType == 1){
            lb1Foot.font = UIFont.boldSystemFont(ofSize: 17.0)
            check1Foot.isHidden = false
        }else if (footType == 2){
            lb2Foot.font = UIFont.boldSystemFont(ofSize: 17.0)
            check2Foot.isHidden = false
        }
    }
    
    // MARK: - IBAction
    @IBAction func pressFootValue(_ sender: UIButton) {
        footType = sender.tag
        updateFootViews()
    }
    
    @IBAction func back(_ sender: Any) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let form = storyboard.instantiateViewController(withIdentifier: "ShoesMeasuresViewController") as! ShoesMeasuresViewController
        form.contentView = contentView
        form.popupData = popupData
        form.product = self.product
        contentView!.show(from: self, to: form)
    }
    
    @IBAction func next(_ sender: Any) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let resultsForm = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        resultsForm.contentView = contentView
        resultsForm.popupData = popupData
        resultsForm.product = self.product
        request?.footType = footType
        resultsForm.request = request
        contentView!.show(from: self, to: resultsForm)
    }
}
