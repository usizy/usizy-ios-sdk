//
//  LatStepViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 01/03/2021.
//

import UIKit

class LastStepViewController: UIViewController {
    let IMAGE_SELECTOR_HEIGHT = CGFloat(260)
    
    @IBOutlet weak var ctContentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnGoBack: UIButton!
    @IBOutlet weak var btnRecommend: UIButton!
    @IBOutlet weak var lbAge: UILabel!
    @IBOutlet weak var lbAgeDescription: UILabel!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var ageTag: UILabel!
    @IBOutlet weak var lbErrorAge: UILabel!
    
    // Chest Male
    @IBOutlet weak var vwMaleChest: UIView!
    @IBOutlet weak var ctHeightMaleChest: NSLayoutConstraint!
    @IBOutlet weak var lbTitleMaleChest: UILabel!
    @IBOutlet weak var lbSubtitleMaleChest: UILabel!
    @IBOutlet weak var iv1MaleChest: UIImageView!
    @IBOutlet weak var lb1MaleChest: UILabel!
    @IBOutlet weak var check1MaleChest: UIImageView!
    @IBOutlet weak var iv2MaleChest: UIImageView!
    @IBOutlet weak var lb2MaleChest: UILabel!
    @IBOutlet weak var check2MaleChest: UIImageView!
    @IBOutlet weak var iv3MaleChest: UIImageView!
    @IBOutlet weak var lb3MaleChest: UILabel!
    @IBOutlet weak var check3MaleChest: UIImageView!
    
    // Chest Woman
    @IBOutlet weak var vwWomanChest: UIView!
    @IBOutlet weak var ctHeightWomanChest: NSLayoutConstraint!
    @IBOutlet weak var lbTitleWomanChest: UILabel!
    @IBOutlet weak var lbSubtitleWomanChest: UILabel!
    @IBOutlet weak var iv1WomanChest: UIImageView!
    @IBOutlet weak var lb1WomanChest: UILabel!
    @IBOutlet weak var check1WomanChest: UIImageView!
    @IBOutlet weak var iv2WomanChest: UIImageView!
    @IBOutlet weak var lb2WomanChest: UILabel!
    @IBOutlet weak var check2WomanChest: UIImageView!
    @IBOutlet weak var iv3WomanChest: UIImageView!
    @IBOutlet weak var lb3WomanChest: UILabel!
    @IBOutlet weak var check3WomanChest: UIImageView!
    
    // Waist Male
    @IBOutlet weak var vwMaleWaist: UIView!
    @IBOutlet weak var ctHeightMaleWaist: NSLayoutConstraint!
    @IBOutlet weak var lbTitleMaleWaist: UILabel!
    @IBOutlet weak var lbSubtitleMaleWaist: UILabel!
    @IBOutlet weak var iv1MaleWaist: UIImageView!
    @IBOutlet weak var lb1MaleWaist: UILabel!
    @IBOutlet weak var check1MaleWaist: UIImageView!
    @IBOutlet weak var iv2MaleWaist: UIImageView!
    @IBOutlet weak var lb2MaleWaist: UILabel!
    @IBOutlet weak var check2MaleWaist: UIImageView!
    @IBOutlet weak var iv3MaleWaist: UIImageView!
    @IBOutlet weak var lb3MaleWaist: UILabel!
    @IBOutlet weak var check3MaleWaist: UIImageView!
    
    // Waist Woman
    @IBOutlet weak var vwWomanWaist: UIView!
    @IBOutlet weak var ctHeightWomanWaist: NSLayoutConstraint!
    @IBOutlet weak var lbTitleWomanWaist: UILabel!
    @IBOutlet weak var lbSubtitleWomanWaist: UILabel!
    @IBOutlet weak var iv1WomanWaist: UIImageView!
    @IBOutlet weak var lb1WomanWaist: UILabel!
    @IBOutlet weak var check1WomanWaist: UIImageView!
    @IBOutlet weak var iv2WomanWaist: UIImageView!
    @IBOutlet weak var lb2WomanWaist: UILabel!
    @IBOutlet weak var check2WomanWaist: UIImageView!
    @IBOutlet weak var iv3WomanWaist: UIImageView!
    @IBOutlet weak var lb3WomanWaist: UILabel!
    @IBOutlet weak var check3WomanWaist: UIImageView!
    
    // Waist Hips
    @IBOutlet weak var vwWomanHips: UIView!
    @IBOutlet weak var ctHeightWomanHips: NSLayoutConstraint!
    @IBOutlet weak var lbTitleWomanHips: UILabel!
    @IBOutlet weak var lbSubtitleWomanHips: UILabel!
    @IBOutlet weak var iv1WomanHips: UIImageView!
    @IBOutlet weak var lb1WomanHips: UILabel!
    @IBOutlet weak var check1WomanHips: UIImageView!
    @IBOutlet weak var iv2WomanHips: UIImageView!
    @IBOutlet weak var lb2WomanHips: UILabel!
    @IBOutlet weak var check2WomanHips: UIImageView!
    @IBOutlet weak var iv3WomanHips: UIImageView!
    @IBOutlet weak var lb3WomanHips: UILabel!
    @IBOutlet weak var check3WomanHips: UIImageView!
    
    // KEYBOARD TOOLBAR
    private var kbToolbar: UIToolbar?
    
    private var selectedSex: String?
    
    private var age: Int = 0

    private var chestType: Int? = nil
    private var waistType: Int? = nil
    private var hipsType: Int? = nil
    
    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    var request: SaveRecommendRequest?
    
    // MARK: - Private vars
    private var limitsMetric: LimitsMetric?
    private var limitsImperial: LimitsImperial?
    private var subjectDataMetric: SubjectDataMetric?
    private var subjectDataImperial: SubjectDataImperial?
    private var sex: String?
    private var sexWithSelectorList: [String] = [SexValues.MALE.rawValue, SexValues.FEMALE.rawValue, SexValues.UNISEX.rawValue]
    private var hideChestImageSelector: Bool = true
    private var hideWaistImageSelector: Bool = true
    private var hideHipsImageSelector: Bool = true
    
    private var sexProfile: Profile?
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariables()
        initViews()
    }
    
    // MARK: - Internal
    private func initVariables() {
        limitsImperial = popupData?.sex_limits?.imperial!
        limitsMetric = popupData?.sex_limits?.metric!
        selectedSex = SharedBetweenFormsData.shared.selectedSex
        sexProfile = SharedBetweenFormsData.shared.getProfile(for: popupData?.sex)
        updateSubjectData()
        age = sexProfile?.age ?? subjectDataMetric?.age ?? 0
        
        let measurements = popupData?.measurements
        sex =  !isNilOrBlank(str: selectedSex) ? selectedSex : popupData?.sex

        let zone = popupData?.zone
        
        let chestMeasurementCondition = (measurements?.contains(MeasurementsValues.CHEST.rawValue))! || zone != ZoneValues.LOWER.rawValue
        let hipsMeasurementCondition = zone == ZoneValues.LOWER.rawValue || (measurements?.contains(MeasurementsValues.HIPS.rawValue))!
        
        if sexWithSelectorList.contains(sex!) && !SharedBetweenFormsData.shared.isFromAdvanced && chestMeasurementCondition {
            hideChestImageSelector = false
        }
        
        if sexWithSelectorList.contains(sex!) && !SharedBetweenFormsData.shared.isFromAdvanced {
            hideWaistImageSelector = false
        }
        
        if sex == SexValues.FEMALE.rawValue && !SharedBetweenFormsData.shared.isFromAdvanced && hipsMeasurementCondition {
            hideHipsImageSelector = false
        }
        
        chestType = sexProfile?.chest_type ?? (SharedBetweenFormsData.shared.isImperial ? subjectDataImperial?.chest_type : subjectDataMetric?.chest_type)
        waistType = sexProfile?.waist_type ?? (SharedBetweenFormsData.shared.isImperial ? subjectDataImperial?.waist_type : subjectDataMetric?.waist_type)
        hipsType = sexProfile?.hips_type ?? (SharedBetweenFormsData.shared.isImperial ? subjectDataImperial?.hips_type : subjectDataMetric?.hips_type)
    }
    
    private func initViews() {
        btnGoBack.setTitle(localizedStringBundle(key: "buttons.return", for: self.classForCoder).uppercased(), for: .normal)
        btnRecommend.setTitle(localizedStringBundle(key: "buttons.recommendme", for: self.classForCoder).uppercased(), for: .normal)
        setUpKeyboardToolbar()
        setupAgeInput()
        
        initImageSelectors()
        initValuesImageSelectors()
    }
    
    private func setupAgeInput() {
        lbAge.text = localizedStringBundle(key: "labels.age", for: self.classForCoder)
        lbAgeDescription.text = localizedStringBundle(key: "description.age", for: self.classForCoder)
        ageTag.text = localizedStringBundle(key: "labels.years", for: self.classForCoder)
        tfAge.keyboardType = .numberPad
        tfAge.text = String(age)
        lbErrorAge.text = ""
        tfAge.inputAccessoryView = kbToolbar
    }
    
    private func setUpKeyboardToolbar() {
        kbToolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(kbDoneButtonTapped))
        kbToolbar?.setItems([flexSpace, doneButton], animated: true)
        kbToolbar?.sizeToFit()
    }
    
    private func updateSubjectData() {
        let dataMetric: DataMetric = (popupData?.data?.metric!)!
        let dataImperial: DataImperial = (popupData?.data?.imperial!)!
        let sex: String = (popupData?.sex)!
    
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(dataMetric: dataMetric, sex: sex, selectedSex: selectedSex)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(dataImperial: dataImperial, sex: sex, selectedSex: selectedSex)
    }
    
    private func navigateToMainForm() {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let mainForm = storyboard.instantiateViewController(withIdentifier: "MainFormViewController") as! MainFormViewController
        
        mainForm.contentView = contentView
        mainForm.popupData = popupData
        mainForm.product = self.product
        contentView!.show(from: self, to: mainForm)
    }
    
    private func navigateToYourMeasuresForm() {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let measuresForm = storyboard.instantiateViewController(withIdentifier: "YourMeasuresFormViewController") as! YourMeasuresFormViewController
        
        measuresForm.contentView = contentView
        measuresForm.popupData = popupData
        measuresForm.product = self.product
        contentView!.show(from: self, to: measuresForm)
    }
    
    private func navigateToRecommend() {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let resultsForm = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        
        resultsForm.contentView = contentView
        resultsForm.popupData = popupData
        resultsForm.product = self.product
        
        self.request?.age = age
        self.request?.chest_type = chestType
        self.request?.waist_type = waistType
        self.request?.hips_type = hipsType
        
        resultsForm.request = request
        contentView!.show(from: self, to: resultsForm)
    }
    
    @objc func kbDoneButtonTapped() {
        view.endEditing(true)
    }
    
    private func hideImageSelector(view:UIView, heightConstraint: NSLayoutConstraint){
        if (!view.isHidden){
            view.isHidden = true
            view.clipsToBounds = true
            heightConstraint.constant = 0
            view.backgroundColor = UIColor.red
            ctContentViewHeight.constant = ctContentViewHeight.constant-IMAGE_SELECTOR_HEIGHT
        }
    }
    
    // MARK: - ImageSelector methods
    private func initImageSelectors(){
        setImageSelectorVisibilityOnSex()
        
        lbTitleMaleChest.text = localizedStringBundle(key: "title.chest", for: self.classForCoder)
        lbSubtitleMaleChest.text = localizedStringBundle(key: "description.chest", for: self.classForCoder)
        lb1MaleChest.text = localizedStringBundle(key: "labels.chest.size1", for: self.classForCoder)
        lb2MaleChest.text = localizedStringBundle(key: "labels.chest.size2", for: self.classForCoder)
        lb3MaleChest.text = localizedStringBundle(key: "labels.chest.size3", for: self.classForCoder)
        
        lbTitleWomanChest.text = localizedStringBundle(key: "title.chest", for: self.classForCoder)
        lbSubtitleWomanChest.text = localizedStringBundle(key: "description.chest", for: self.classForCoder)
        lb1WomanChest.text = localizedStringBundle(key: "labels.chest.size1", for: self.classForCoder)
        lb2WomanChest.text = localizedStringBundle(key: "labels.chest.size2", for: self.classForCoder)
        lb3WomanChest.text = localizedStringBundle(key: "labels.chest.size3", for: self.classForCoder)
        
        lbTitleMaleWaist.text = localizedStringBundle(key: "title.waist", for: self.classForCoder)
        lbSubtitleMaleWaist.text = localizedStringBundle(key: "description.waist", for: self.classForCoder)
        lb1MaleWaist.text = localizedStringBundle(key: "labels.waist.size1", for: self.classForCoder)
        lb2MaleWaist.text = localizedStringBundle(key: "labels.waist.size2", for: self.classForCoder)
        lb3MaleWaist.text = localizedStringBundle(key: "labels.waist.size3", for: self.classForCoder)
        
        lbTitleWomanWaist.text = localizedStringBundle(key: "title.waist", for: self.classForCoder)
        lbSubtitleWomanWaist.text = localizedStringBundle(key: "description.waist", for: self.classForCoder)
        lb1WomanWaist.text = localizedStringBundle(key: "labels.waist.size1", for: self.classForCoder)
        lb2WomanWaist.text = localizedStringBundle(key: "labels.waist.size2", for: self.classForCoder)
        lb3WomanWaist.text = localizedStringBundle(key: "labels.waist.size3", for: self.classForCoder)
        
        lbTitleWomanHips.text = localizedStringBundle(key: "title.hips", for: self.classForCoder)
        lbSubtitleWomanHips.text = localizedStringBundle(key: "description.hips", for: self.classForCoder)
        lb1WomanHips.text = localizedStringBundle(key: "labels.hips.size1", for: self.classForCoder)
        lb2WomanHips.text = localizedStringBundle(key: "labels.hips.size2", for: self.classForCoder)
        lb3WomanHips.text = localizedStringBundle(key: "labels.hips.size3", for: self.classForCoder)
    }
    
    private func initValuesImageSelectors() {
        if chestType ?? 0 > 0 {
            updateMaleChestViews()
            updateWomanChestViews()
        }
        if waistType ?? 0 > 0 {
            updateMaleWaistViews()
            updateWomanWaistViews()
        }
        if hipsType ?? 0 > 0 {
            updateWomanHipsViews()
        }
    }
    
    private func hideAllMaleImageSelectors() {
        hideMaleWaist()
        hideMaleChest()
    }
    
    private func hideAllFemaleImageSelectors() {
        hideWomanChest()
        hideWomanHips()
        hideWomanWaist()
    }
    
    private func setImageSelectorVisibilityOnSex() {
        
        if !sexWithSelectorList.contains(sex!) {
            hideAllMaleImageSelectors()
            hideAllFemaleImageSelectors()
            return
        } else if sex == SexValues.MALE.rawValue {
            hideAllFemaleImageSelectors()
        } else if sex == SexValues.FEMALE.rawValue {
            hideAllMaleImageSelectors()
        }
        
        if hideChestImageSelector {
                hideMaleChest()
                hideWomanChest()
        }
        
        if hideWaistImageSelector {
                hideMaleWaist()
                hideWomanWaist()
        }
        
        if hideHipsImageSelector {
            hideWomanHips()
        }
    }
    
    private func hideMaleChest() {
        hideImageSelector(view: vwMaleChest, heightConstraint: ctHeightMaleChest)
    }
    
    private func hideWomanChest() {
        hideImageSelector(view: vwWomanChest, heightConstraint: ctHeightWomanChest)
    }
    
    private func hideMaleWaist() {
        hideImageSelector(view: vwMaleWaist, heightConstraint: ctHeightMaleWaist)
    }
    
    private func hideWomanWaist() {
        hideImageSelector(view: vwWomanWaist, heightConstraint: ctHeightWomanWaist)
    }
    
    private func hideWomanHips() {
        hideImageSelector(view: vwWomanHips, heightConstraint: ctHeightWomanHips)
    }
    
    private func updateImageSelectorViewsWithValue(_ value: Int?, lb1:UILabel, lb2:UILabel, lb3:UILabel, check1: UIImageView, check2: UIImageView, check3: UIImageView){
        lb1.font = UIFont.systemFont(ofSize: 17.0)
        lb2.font = UIFont.systemFont(ofSize: 17.0)
        lb3.font = UIFont.systemFont(ofSize: 17.0)
        check1.isHidden = true
        check2.isHidden = true
        check3.isHidden = true
        
        if (value == 1){
            lb1.font = UIFont.boldSystemFont(ofSize: 17.0)
            check1.isHidden = false
        }else if (value == 2){
            lb2.font = UIFont.boldSystemFont(ofSize: 17.0)
            check2.isHidden = false
        }else if (value == 3){
            lb3.font = UIFont.boldSystemFont(ofSize: 17.0)
            check3.isHidden = false
        }
    }
    
    private func updateMaleChestViews(){
        updateImageSelectorViewsWithValue(
            chestType,
            lb1: lb1MaleChest,
            lb2: lb2MaleChest,
            lb3: lb3MaleChest,
            check1: check1MaleChest,
            check2: check2MaleChest,
            check3: check3MaleChest)
    }
    
    private func updateWomanChestViews(){
        updateImageSelectorViewsWithValue(
            chestType,
            lb1: lb1WomanChest,
            lb2: lb2WomanChest,
            lb3: lb3WomanChest,
            check1: check1WomanChest,
            check2: check2WomanChest,
            check3: check3WomanChest)
    }
    
    private func updateMaleWaistViews(){
        updateImageSelectorViewsWithValue(
            waistType,
            lb1: lb1MaleWaist,
            lb2: lb2MaleWaist,
            lb3: lb3MaleWaist,
            check1: check1MaleWaist,
            check2: check2MaleWaist,
            check3: check3MaleWaist)
    }
    
    private func updateWomanWaistViews(){
        updateImageSelectorViewsWithValue(
            waistType,
            lb1: lb1WomanWaist,
            lb2: lb2WomanWaist,
            lb3: lb3WomanWaist,
            check1: check1WomanWaist,
            check2: check2WomanWaist,
            check3: check3WomanWaist)
    }
    
    private func updateWomanHipsViews(){
        updateImageSelectorViewsWithValue(
            hipsType,
            lb1: lb1WomanHips,
            lb2: lb2WomanHips,
            lb3: lb3WomanHips,
            check1: check1WomanHips,
            check2: check2WomanHips,
            check3: check3WomanHips)
    }
    
    // MARK: - IBActions Age Input
    @IBAction func onAgeDidEdit(_ sender: UITextField) {
        let value = sender.text!
        let min: Float = SharedBetweenFormsData.shared.isImperial ? (limitsImperial?.age?.min!)! : (limitsMetric?.age?.min!)!
        let max: Float = SharedBetweenFormsData.shared.isImperial ? (limitsImperial?.age?.max!)! : (limitsMetric?.age?.max!)!
        if FormValidationHelper.isOutOfValidRange(value: value, min: min, max: max) {
            lbErrorAge.text = "\(Int(min)) - \(Int(max))"
        } else {
            lbErrorAge.text = ""
            age = Int(value) ?? 0
            sexProfile?.age = age
        }
    }
    
    @IBAction func onAgeChange(_ sender: UITextField) {
        let value = sender.text!
        let min: Float = SharedBetweenFormsData.shared.isImperial ? (limitsImperial?.age?.min!)! : (limitsMetric?.age?.min!)!
        let max: Float = SharedBetweenFormsData.shared.isImperial ? (limitsImperial?.age?.max!)! : (limitsMetric?.age?.max!)!
        
        if FormValidationHelper.isOutOfValidRange(value: value, min: min, max: max) {
            lbErrorAge.text = "\(Int(min)) - \(Int(max))"
        } else {
            lbErrorAge.text = ""
            age = Int(value) ?? 0
            sexProfile?.age = age
        }
    }
    
    @IBAction func onBtnRecommendPress(_ sender: UIButton) {
        var areVisibleSelectorsWithValue = false
        
        if !hideChestImageSelector {
            areVisibleSelectorsWithValue = chestType ?? 0 > 0
        }
        
        if !hideWaistImageSelector {
            areVisibleSelectorsWithValue = waistType ?? 0 > 0
        }
        
        if !hideHipsImageSelector {
            areVisibleSelectorsWithValue = hipsType ?? 0 > 0
        }
        
        if (!sexWithSelectorList.contains(sex!)) {
            areVisibleSelectorsWithValue = true
        }
        
        if (!FormValidationHelper.someHasError(labels: lbErrorAge) && (areVisibleSelectorsWithValue || SharedBetweenFormsData.shared.isFromAdvanced)) {
            navigateToRecommend()
        } else {
            print("not valid range")
        }
    }
    
    @IBAction func onBtnBackPress(_ sender: UIButton) {
        if SharedBetweenFormsData.shared.isFromAdvanced {
            navigateToYourMeasuresForm()
        } else {
            navigateToMainForm()
        }
    }

    // MARK: - IBActions Image Selectors
    @IBAction func pressMaleChestValue(_ sender: UIButton) {
        chestType = sender.tag
        sexProfile?.chest_type = chestType
        updateMaleChestViews()
    }
    
    @IBAction func pressWomanChestValue(_ sender: UIButton) {
        chestType = sender.tag
        sexProfile?.chest_type = chestType
        updateWomanChestViews()
    }
    
    @IBAction func pressMaleWaistValue(_ sender: UIButton) {
        waistType = sender.tag
        sexProfile?.waist_type = chestType
        updateMaleWaistViews()
    }
    
    @IBAction func pressWomanWaistValue(_ sender: UIButton) {
        waistType = sender.tag
        sexProfile?.waist_type = chestType
        updateWomanWaistViews()
    }
    
    @IBAction func pressHipsValue(_ sender: UIButton) {
        hipsType = sender.tag
        sexProfile?.hips_type = chestType
        updateWomanHipsViews()

    }
    
    @IBAction func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            textField.selectAll(nil)
        }
    }
}
