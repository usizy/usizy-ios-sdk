//
//  ErrorViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 03/03/2021.
//

import UIKit

class ErrorViewController: UIViewController {

    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    var overflowMessage: String?
    var shoes: Bool = false
    
    // MARK: - IBOutlet
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var btStart: UIButton!
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbMessage.text = overflowMessage ?? localizedStringBundle(key: "error.tryagain", for: self.classForCoder)
        self.btStart.setTitle(localizedStringBundle(key: "buttons.redo", for: self.classForCoder).uppercased(), for: .normal)
    }

    @IBAction func gotoStart(_ sender: Any) {
        if (shoes){
            let storyboard = loadStoryboardBundle(for: self.classForCoder)
            let vc = storyboard.instantiateViewController(withIdentifier: "ShoesFormViewController") as! ShoesFormViewController
            vc.contentView = contentView
            vc.popupData = popupData
            vc.product = self.product
            contentView!.show(from: self, to: vc)
        }else{
            let storyboard = loadStoryboardBundle(for: self.classForCoder)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainFormViewController") as! MainFormViewController
            vc.contentView = contentView
            vc.popupData = popupData
            vc.product = self.product
            contentView!.show(from: self, to: vc)
        }

    }
}
