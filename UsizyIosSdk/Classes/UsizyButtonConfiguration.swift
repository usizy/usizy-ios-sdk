//
//  UsizyButtonConfiguration.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 26/02/2021.
//

import Foundation
import UIKit

public class UsizyButtonConfiguration {
    public var productId: String?
    public var user: String?
    public var logoResName: String?
    public var iconResName: String?
    public var title: String?
    public var titleColor: UIColor?
    public var fontFamily: UIFont?
    public init() { }
}
