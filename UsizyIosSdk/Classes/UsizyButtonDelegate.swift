//
//  UsizyButtonDelegate.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 26/02/2021.
//

import Foundation

public protocol UsizyButtonDelegate {
    func onRecommendedSize(_ productId: String?, data: SaveRecommendResponse?)
}
