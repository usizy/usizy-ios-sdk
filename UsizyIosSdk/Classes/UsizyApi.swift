//
//  UsizyApi.swift
//  Pods
//
//  Created by Pedro Aquilino on 20/8/24.
//

import Foundation

public class UsizyApi {
    public static func confirm (_ parameters: ConfirmRequest, completion: @escaping ((ConfirmResponseErrors?) -> Void)) {
        SizeQuery().confirm(parameters, completion: completion)
    }
}
