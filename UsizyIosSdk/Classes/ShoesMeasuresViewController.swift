//
//  ShoesMeasuresViewController.swift
//  UsizyIosSdk
//
//  Created by Daniel Almería Núñez on 19/4/21.
//

import UIKit

class ShoesMeasuresViewController: UIViewController {

    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var btBasic: UIButton!
    @IBOutlet weak var btNext: UIButton!
    @IBOutlet weak var lbFootTitle: UILabel!
    @IBOutlet weak var lbFootValue: UILabel!
    @IBOutlet weak var vwSelect: UIView!
    @IBOutlet weak var swMetricSystem: UISwitch!
    
    // MARK: - Internal vars
    private var isMetric: Bool = true
    private var options: [String] = []
    private var alert: UIAlertController = UIAlertController()
    
    private var dataMetric: SubjectDataMetric? = nil
    private var dataImperial: SubjectDataImperial? = nil
    private var footLimitSelectedMetric: Float = 0.0
    private var footLimitSelectedImperial: Float = 0.0
    
    // MARK: - Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
    }
    
    // MARK: - Views and variables Initializers
    private func initVariables() {
        let sdataMetric: DataMetric = (popupData?.data?.metric!)!
        let sdataImperial: DataImperial = (popupData?.data?.imperial!)!
        let sex: String = (popupData?.sex)!
        dataMetric = DataExtractionUtils.getSubjectDataBySex(dataMetric: sdataMetric, sex: sex, selectedSex: nil)
        dataImperial = DataExtractionUtils.getSubjectDataBySex(dataImperial: sdataImperial, sex: sex, selectedSex: nil)
    }
    
    private func initViews() {
        btBasic.titleLabel?.lineBreakMode = .byWordWrapping
        btBasic.titleLabel?.numberOfLines = 2
        btBasic.underlineButton(text: localizedStringBundle(key: "buttons.basic", for: self.classForCoder))
        
        btNext.setTitle(localizedStringBundle(key: "buttons.continue", for: self.classForCoder).uppercased(), for: .normal)
        
        lbFootTitle.text = localizedStringBundle(key: "measurements.foot", for: self.classForCoder)
        
        initOptions()
    }
    
    // MARK: - IBAction
    @IBAction func showBasic(_ sender: Any) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let mainForm = storyboard.instantiateViewController(withIdentifier: "ShoesFormViewController") as! ShoesFormViewController
        mainForm.contentView = contentView
        mainForm.popupData = popupData
        mainForm.product = self.product
        contentView!.show(from: self, to: mainForm)
    }
    
    @IBAction func next(_ sender: Any) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let form = storyboard.instantiateViewController(withIdentifier: "ShoesImageSelectorViewController") as! ShoesImageSelectorViewController
        form.contentView = contentView
        form.popupData = popupData
        form.product = self.product
        form.request = getSaveRecommendRequest()
        contentView!.show(from: self, to: form)
    }
    
    @IBAction func showOptions(_ sender: Any) {
           self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onSystemMetricChange(_ sender: UISwitch) {
        isMetric = !swMetricSystem.isOn
        SharedBetweenFormsData.shared.isImperial = !isMetric
        initOptions()
    }
    
    // MARK: - Internal
    private func getSaveRecommendRequest() -> SaveRecommendRequest{
        let request = SaveRecommendRequest(product: product)

        request.sid = popupData?.sid
        request.ck = popupData?.ck
        
        if (popupData?.sex == SexValues.FEMALE.rawValue){
            request.sex = popupData?.sex
        }  else {
            request.sex = SexValues.MALE.rawValue
        }
        
        request.w = RecommendationWay.ADV_WAY.rawValue
        request.foot = isMetric ? footLimitSelectedMetric : footLimitSelectedImperial

        if (isMetric){
            if (popupData?.sex == SexValues.FEMALE.rawValue){
                request.age = popupData?.data?.metric?.female?.age
                request.footType = popupData?.data?.metric?.female?.foot_type
                request.footWidth = popupData?.data?.metric?.female?.foot_width
            }else{
                request.age = popupData?.data?.metric?.male?.age
                request.footType = popupData?.data?.metric?.male?.foot_type
                request.footWidth = popupData?.data?.metric?.male?.foot_width
            }
            request.metric_system = MetricSystem.METRIC.rawValue
        }else{
            if (popupData?.sex == SexValues.FEMALE.rawValue){
                request.age = popupData?.data?.imperial?.female?.age
                request.footType = popupData?.data?.imperial?.female?.foot_type
                request.footWidth = popupData?.data?.imperial?.female?.foot_width
            }else{
                request.age = popupData?.data?.imperial?.male?.age
                request.footType = popupData?.data?.imperial?.male?.foot_type
                request.footWidth = popupData?.data?.imperial?.male?.foot_width
            }
            request.metric_system = MetricSystem.IMPERIAL.rawValue
        }
        return request
    }
    
    private func initOptions(){
        var config: MeasureLimitMetric? = nil
        var suffix = ""
        
        if (isMetric) {
            config = popupData?.sex_limits?.metric?.foot
            suffix = "cm"
        } else {
            config = popupData?.sex_limits?.imperial?.foot
            suffix = "in"
        }
        
        if (config != nil){

            options = []

            let max = config!.max!
            let min = config!.min!
            let step = config!.step!

            var i = min
            while (i < max) {
                options.append("\(i) \(suffix)")
                i = round((i + step) * 100) / 100 //Fix float round
            }

            if (options.count > 0){
                
                var index: Int? = nil
                if (isMetric){
                    index = options.firstIndex(of: "\(footLimitSelectedMetric) \(suffix)")
                }else{
                    index = options.firstIndex(of: "\(footLimitSelectedImperial) \(suffix)")
                }
                    
                let position = index == nil ? 0 : index!
                vwSelect.isHidden = false
                lbFootValue.text = options[position]
                if (isMetric){
                    footLimitSelectedMetric = min + (Float(position) * step)
                }else{
                    footLimitSelectedImperial = min + (Float(position) * step)
                }
                
                initAlertOptions()
            }else{
                vwSelect.isHidden = true
            }
        }else{
            vwSelect.isHidden = true
        }
    }
    
    private func initAlertOptions(){
        alert = UIAlertController(
            title: localizedStringBundle(key: "buttons.select", for: self.classForCoder),
            message: localizedStringBundle(key: "measurements.foot", for: self.classForCoder),
            preferredStyle: .actionSheet)
           
        for (position, option) in options.enumerated() {
                alert.addAction(UIAlertAction(title: option, style: .default , handler:{(UIAlertAction) in
                    var config: MeasureLimitMetric? = nil
                    if (self.isMetric) {
                        config = self.popupData?.sex_limits?.metric?.foot
                    } else {
                        config = self.popupData?.sex_limits?.imperial?.foot
                    }
                    
                    if (config != nil){
                        let min = config!.min!
                        let step = config!.step!
                        self.lbFootValue.text = self.options[position]
                        
                        if (self.isMetric){
                            self.footLimitSelectedMetric = min + (Float(position) * step)
                        }else{
                            self.footLimitSelectedImperial = min + (Float(position) * step)
                        }
                    }
                }))
            }
          
        alert.addAction(UIAlertAction(title: localizedStringBundle(key: "buttons.return", for: self.classForCoder), style: .cancel, handler: nil))
    }
}

