//
//  SizeQuery.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 02/03/2021.
//

import Foundation

class SizeQuery {
    
    let anonymousUser = "AnonymousUser"
    
    public func displayButton(_ parameters: DisplayButtonRequest, completion: @escaping ((DisplayButtonResponse?) -> Void)){
        
        if (parameters.ref == nil){
            parameters.ref = Bundle.main.bundleIdentifier
        }
        
        if (parameters.language == nil){
            parameters.language = NSLocale.current.languageCode
        }
        
        parameters.user = SizeCache.shared.cacheUser ?? parameters.user ?? anonymousUser
        
        let displayButtonRequest = NetworkServiceRoute.displayButton(parameters: parameters)
        Network.send(request: displayButtonRequest) { (response: Result<DisplayButtonResponse, Error>) in
            do {
                let value = try response.get()
                completion(value)
            } catch {
                print("Unexpected error: \(error).")
                completion(nil)
            }
        }
    }
    
    public func openPopup(_ parameters: OpenPopupRequest, completion: @escaping ((OpenPopupResponse?) -> Void)){
        
        if (parameters.ref == nil){
            parameters.ref = Bundle.main.bundleIdentifier
        }
        
        if (parameters.language == nil){
            parameters.language = NSLocale.current.languageCode
        }
        
        parameters.sid = SizeCache.shared.sid
        parameters.sk = SizeCache.shared.sk
        parameters.user = SizeCache.shared.cacheUser ?? parameters.user ?? anonymousUser
        
        let openPopupRequest = NetworkServiceRoute.openPopup(parameters: parameters)
        Network.send(request: openPopupRequest) { (response: Result<OpenPopupResponse, Error>) in
            do {
                let value = try response.get()
                completion(value)
            } catch {
                print("Unexpected error: \(error).")
                completion(nil)
            }
        }
    }
    
    public func saveRecommend(_ parameters: SaveRecommendRequest, completion: @escaping ((SaveRecommendResponse?) -> Void)){
        
        if (parameters.ref == nil){
            parameters.ref = Bundle.main.bundleIdentifier
        }
        
        if (parameters.language == nil){
            parameters.language = NSLocale.current.languageCode
        }
        
        parameters.sid = SizeCache.shared.sid
        parameters.sk = SizeCache.shared.sk
        parameters.user = SizeCache.shared.cacheUser ?? parameters.user ?? anonymousUser
        
        let saveRecommendRequest = NetworkServiceRoute.saveRecommend(parameters: parameters)
        Network.send(request: saveRecommendRequest) { (response: Result<SaveRecommendResponse, Error>) in
            do {
                let value = try response.get()
                completion(value)
            } catch {
                print("Unexpected error: \(error).")
                completion(nil)
            }
        }
    }
    
    public func shoeBrand(_ parameters: ShoeBrandRequest, completion: @escaping ((ShowBrandResponse?) -> Void)){
        
        if (parameters.ref == nil){
            parameters.ref = Bundle.main.bundleIdentifier
        }
        
        if (parameters.language == nil){
            parameters.language = NSLocale.current.languageCode
        }
        
        if (parameters.limit == nil){
            parameters.limit = 12
        }
        
        parameters.sid = SizeCache.shared.sid
        parameters.sk = SizeCache.shared.sk
        parameters.user = SizeCache.shared.cacheUser ?? parameters.user ?? anonymousUser
        
        let shoeBrandRequest = NetworkServiceRoute.shoeBrand(parameters: parameters)
        Network.send(request: shoeBrandRequest) { (response: Result<ShowBrandResponse, Error>) in
            do {
                let value = try response.get()
                completion(value)
            } catch {
                print("Unexpected error: \(error).")
                completion(nil)
            }
        }
    }
    
    public func confirm (_ parameters: ConfirmRequest, completion: @escaping ((ConfirmResponseErrors?) -> Void)) {
        if (parameters.ref == nil){
            parameters.ref = Bundle.main.bundleIdentifier
        }
        if (parameters.language == nil){
            parameters.language = NSLocale.current.languageCode
        }
        parameters.sk = SizeCache.shared.sk
        parameters.user = SizeCache.shared.cacheUser ?? parameters.user ?? anonymousUser
        
        let confirmRequest = NetworkServiceRoute.confirm(parameters: parameters)
        Network.send_api(request: confirmRequest) { (response: Result<String, Error>) in
            switch response {
            case .success:
                completion(nil)
            case .failure(let error):
                if let errors = error as? ConfirmResponseErrors {
                    completion(errors)
                } else {
                    let errors = ConfirmResponseErrors()
                    errors.non_field_errors = [error.localizedDescription]
                    completion(errors)
                }
            }
        }
    }
}

class DisplayButtonRequest: Encodable {
    init(product: String?) {
        self.product = product
    }
    
    var product: String? = nil
    var user: String? = nil
    var ref: String? = nil
    var language: String? = nil
}

class DisplayButtonResponse: Decodable {
    var show: ShowProduct
    var sid: String? = nil
    var sk: String? = nil
}

class OpenPopupRequest: Encodable {
    init(product: String?) {
        self.product = product
    }
    
    var sid: String? = nil
    var sk: String? = nil
    
    var product: String? = nil
    var user: String? = nil
    var ref: String? = nil
    var language: String? = nil
}

class OpenPopupResponse: Decodable {
    var sid: String? = nil
    var ck: String? = nil
    var sex: String = ""
    var branding: String = ""
    var zone: String = ""
    var only_height: Bool = false
    var logo: String? = nil
    var image: String? = nil
    var metric_system: String? = nil
    var mass_system: String? = nil
    var measurements: [String]? = nil
    var sex_limits: SexLimits? = nil
    var data: DataPopup? = nil
    var shoe_favs: [ShoeFavs]? = nil
    var shoe_system: String? = nil
}

class SaveRecommendRequest: Encodable {
    init(product: String?) {
        self.product = product
    }
    
    var sid: String? = nil
    var sk: String? = nil
    
    var product: String? = nil
    var user: String? = nil
    var ref: String? = nil
    var language: String? = nil
    
    var age: Int? = nil
    var arm: String? = nil
    var arm_type: Int? = nil
    var arm_width: String? = nil
    var back: String? = nil
    var bike_fit: Int? = nil
    var chest: String? = nil
    var chest_type: Int? = nil
    var ck: String? = nil
    var crotch: String? = nil
    var fit: Int? = nil
    var flexibility: Int? = nil
    var foot: Float? = nil
    var footType: Int? = nil
    var footWidth: Int? = nil
    var halfArmspan: String? = nil
    var hand: String? = nil
    var hand_perimeter: String? = nil
    var head: String? = nil
    var height: String? = nil
    var hips: String? = nil
    var hips_type: Int? = nil
    var inseam: String? = nil
    var leg_type: Int? = nil
    var length: String? = nil
    var long_inseam: String? = nil
    var mass_system: String? = nil
    var metric_system: String? = nil
    var neck: String? = nil
    var outside_leg: String? = nil
    var reach: String? = nil
    var sex: String? = nil
    var shoe_brand: Int? = nil
    var shoe_size: String? = nil
    var shoulder: String? = nil
    var sizesystem: String? = nil
    var stack: String? = nil
    var stockSizes: [String]? = nil
    var thigh: String? = nil
    var w: String? = nil
    var waist: String? = nil
    var waist_type: Int? = nil
    var weight: String? = nil
    var wrist: String? = nil
}

class ShoeBrandRequest: Encodable {
    init(product: String?) {
        self.product = product
    }
    
    var sid: String? = nil
    var sk: String? = nil
    
    var product: String? = nil
    var user: String? = nil
    var ref: String? = nil
    var language: String? = nil
    
    var sex: String? = nil
    var q: String? = nil
    var limit: Int? = nil
}

public class SaveRecommendResponse: Decodable {
    public var branding: String? = nil
    public var extra: [String: String]? = nil
    public var status: String? = nil
    public var usizy_quicksize_text: String? = nil
    public var usizy_in_stock: Bool? = nil
    public var usizy_size: String? = nil
    public var usizy_size_country: String? = nil
    public var usizy_size_country_system: String? = nil
    public var usizy_size_result: String? = nil
}

class SexLimits: Decodable {
    var imperial: LimitsImperial? = nil
    var metric: LimitsMetric? = nil
}

class LimitsImperial: Decodable {
    var age: MeasureLimitMetric? = nil
    var chest: MeasureLimitMetric? = nil
    var height: HeightLimitImperial? = nil
    var hips: MeasureLimitMetric? = nil
    var waist: MeasureLimitMetric? = nil
    var weight: MeasureLimitMetric? = nil
    var foot: MeasureLimitMetric? = nil
}

class LimitsMetric: Decodable {
    var age: MeasureLimitMetric? = nil
    var chest: MeasureLimitMetric? = nil
    var height: MeasureLimitMetric? = nil
    var hips: MeasureLimitMetric? = nil
    var waist: MeasureLimitMetric? = nil
    var weight: MeasureLimitMetric? = nil
    var foot: MeasureLimitMetric? = nil
}

class MeasureLimitMetric: Decodable {
    var max: Float? = nil
    var min: Float? = nil
    var step: Float? = nil
}

class HeightLimitImperial: Decodable {
    var max: HeightImperial? = nil
    var min: HeightImperial? = nil
    var step: Float? = nil
}

class HeightImperial: Decodable {
    init(ft: Float?, inX: Float?) {
        self.ft = ft
        self.inX = inX
    }
    
    var ft: Float? = nil
    var inX: Float? = nil
    
    enum CodingKeys: String, CodingKey {
        case inX = "in"
        case ft
    }
}

class DataPopup: Decodable {
    var imperial: DataImperial? = nil
    var metric: DataMetric? = nil
}

class DataImperial: Decodable {
    var baby: SubjectDataImperial? = nil
    var boy: SubjectDataImperial? = nil
    var child: SubjectDataImperial? = nil
    var male: SubjectDataImperial? = nil
    var female: SubjectDataImperial? = nil
    var girl: SubjectDataImperial? = nil
}

class DataMetric: Decodable {
    var baby: SubjectDataMetric? = nil
    var boy: SubjectDataMetric? = nil
    var child: SubjectDataMetric? = nil
    var male: SubjectDataMetric? = nil
    var female: SubjectDataMetric? = nil
    var girl: SubjectDataMetric? = nil
}

class SubjectDataImperial: Decodable {
    var age: Int? = nil
    var arm: Int? = nil
    var arm_type: Int? = nil
    var arm_width: Int? = nil
    var back: Int? = nil
    var bike_fit: Int? = nil
    var chest: Float? = nil
    var chest_type: Int? = nil
    var crotch: Int? = nil
    var fit: Int? = nil
    var flexibility: Int? = nil
    var foot: Float? = nil
    var foot_type: Int? = nil
    var foot_width: Int? = nil
    var half_armspan: Int? = nil
    var hand: Int? = nil
    var hand_perimeter: Int? = nil
    var height: HeightImperial? = nil
    var hips: Float? = nil
    var hips_type: Int? = nil
    var inseam: Int? = nil
    var leg_type: Int? = nil
    var length: Int? = nil
    var long_inseam: Int? = nil
    var lower_waist: Int? = nil
    var morphology: Int? = nil
    var neck: Int? = nil
    var outside_leg: Int? = nil
    var reach: Int? = nil
    var shoulder: Int? = nil
    var stack: Int? = nil
    var thigh: Int? = nil
    var waist: Float? = nil
    var waist_type: Int? = nil
    var weight: Float? = nil
    var wrist: Int? = nil
}

class SubjectDataMetric: Decodable {
    var age: Int? = nil
    var arm: Int? = nil
    var arm_type: Int? = nil
    var arm_width: Int? = nil
    var back: Int? = nil
    var bike_fit: Int? = nil
    var chest: Float? = nil
    var chest_type: Int? = nil
    var crotch: Int? = nil
    var fit: Int? = nil
    var flexibility: Int? = nil
    var foot: Float? = nil
    var foot_type: Int? = nil
    var foot_width: Int? = nil
    var half_armspan: Int? = nil
    var hand: Int? = nil
    var hand_perimeter: Int? = nil
    var height: Float? = nil
    var hips: Float? = nil
    var hips_type: Int? = nil
    var inseam: Int? = nil
    var leg_type: Int? = nil
    var length: Int? = nil
    var long_inseam: Int? = nil
    var lower_waist: Int? = nil
    var morphology: Int? = nil
    var neck: Int? = nil
    var outside_leg: Int? = nil
    var reach: Int? = nil
    var shoulder: Int? = nil
    var stack: Int? = nil
    var thigh: Int? = nil
    var waist: Float? = nil
    var waist_type: Int? = nil
    var weight: Float? = nil
    var wrist: Int? = nil
}

public class ShowBrandResponse: Decodable {
    var status: String? = nil
    var results: [ShoeFavs]? = nil
}

public enum ShoeFavsSizes: Decodable {
    case origin(String), conversion([String])
    
    public init(from decoder: Decoder) throws {
        if let simple = try? decoder.singleValueContainer().decode(String.self) {
            self = .origin(simple)
            return
        }
        
        if let list = try? decoder.singleValueContainer().decode([String].self) {
            self = .conversion(list)
            return
        }
        
        throw ShoeFavsSizes.missingValue
    }
    
    enum ShoeFavsSizes:Error {
        case missingValue
    }
}

extension ShoeFavsSizes {
    
    var origin: String? {
        switch self {
        case .origin(let value): return value
        case .conversion: return nil
        }
    }
    
    var conversion: [String]? {
        switch self {
        case .origin: return nil
        case .conversion(let value): return value
        }
    }
}

public class ShoeFavs: Decodable {
    var id: Int? = nil
    var text: String? = nil
    var thumb: String? = nil
    var sizes: [String:[[ShoeFavsSizes]]]? = nil
}

public class ShoeSizes {
    var region: String? = nil
    var values: [String]? = nil
}

public enum ConfirmValidationError: Error {
    case missingOrderId
    case missingProductIds
    case missingSizes
}

public class ConfirmRequest: Encodable {
    public init(
        order_id: String,
        product_ids: [String],
        variation_ids: [String]? = nil,
        sizes: [String],
        sizes_system: [String]? = nil,
        currency: String? = nil,
        prices_vat: [String]? = nil,
        prices_no_vat: [String]? = nil,
        total_vat: String? = nil,
        total_no_vat: String? = nil,
        shipping_cost: String? = nil
    ) throws {
        self.order_id = order_id
        self.product_ids = product_ids
        self.variation_ids = variation_ids
        self.sizes = sizes
        self.sizes_system = sizes_system
        self.currency = currency
        self.prices_vat = prices_vat
        self.prices_no_vat = prices_no_vat
        self.total_vat = total_vat
        self.total_no_vat = total_no_vat
        self.shipping_cost = shipping_cost
        try self.validate()
    }
    
    var order_id: String
    var product_ids: [String] = []
    var variation_ids: [String]? = nil
    var sizes: [String] = []
    var sizes_system: [String]? = nil
    var currency: String? = nil
    var prices_vat: [String]? = nil
    var prices_no_vat: [String]? = nil
    var total_vat: String? = nil
    var total_no_vat: String? = nil
    var shipping_cost: String? = nil
    
    var sk: String? = nil
    var user: String? = nil
    var ref: String? = nil
    var language: String? = nil
    
    private func validate() throws {
        guard !self.order_id.isEmpty else {
            throw ConfirmValidationError.missingOrderId
        }
        guard !self.product_ids.isEmpty else {
            throw ConfirmValidationError.missingProductIds
        }
        guard !self.sizes.isEmpty else {
            throw ConfirmValidationError.missingSizes
        }
    }
}

public class ConfirmResponseErrors: Decodable, Error {
    var non_field_errors: [String]? = nil
    var ecommerce: [String]? = nil
    var order_id: [String]? = nil
    var product_ids: [String]? = nil
    var variation_ids: [String]? = nil
    var sizes: [String]? = nil
    var sizes_system: [String]? = nil
    var prices_no_vat: [String]? = nil
    var prices_vat: [String]? = nil
    var total_no_vat: [String]? = nil
    var total_vat: [String]? = nil
    var shipping_cost: [String]? = nil
    var currency: [String]? = nil
}

extension ConfirmResponseErrors: CustomStringConvertible {
    
   public var description: String {
       var desc = ""
       if non_field_errors != nil {
           desc += "non_field_errors: " + non_field_errors!.joined(separator: ", ")
       }
       if ecommerce != nil {
           desc += "ecommerce: " + ecommerce!.joined(separator: ", ")
       }
       if order_id != nil {
           desc += "order_id: " + order_id!.joined(separator: ", ")
       }
       if product_ids != nil {
           desc += "product_ids: " + product_ids!.joined(separator: ", ")
       }
       if variation_ids != nil {
           desc += "variation_ids: " + variation_ids!.joined(separator: ", ")
       }
       if sizes != nil {
           desc += "sizes: " + sizes!.joined(separator: ", ")
       }
       if sizes_system != nil {
           desc += "sizes_system: " + sizes_system!.joined(separator: ", ")
       }
       if prices_no_vat != nil {
           desc += "prices_no_vat: " + prices_no_vat!.joined(separator: ", ")
       }
       if prices_vat != nil {
           desc += "prices_vat: " + prices_vat!.joined(separator: ", ")
       }
       if total_no_vat != nil {
           desc += "total_no_vat: " + total_no_vat!.joined(separator: ", ")
       }
       if total_vat != nil {
           desc += "total_vat: " + total_vat!.joined(separator: ", ")
       }
       if shipping_cost != nil {
           desc += "shipping_cost: " + shipping_cost!.joined(separator: ", ")
       }
       if currency != nil {
           desc += "currency: " + currency!.joined(separator: ", ")
       }
       return desc
   }
}
