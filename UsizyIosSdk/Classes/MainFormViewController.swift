//
//  MainFormViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 26/02/2021.
//

import UIKit

class MainFormViewController: UIViewController {
    private let INPUT_HEIGHT = CGFloat(120)
    
    // MARK: - IBOutlet
    @IBOutlet weak var ivProduct: UIImageView!
    @IBOutlet weak var tvTitle: UITextView!
    @IBOutlet weak var swMetricSystem: UISwitch!
    
    // METRIC
    @IBOutlet weak var lbHeightMetric: UILabel!
    @IBOutlet weak var ctMetrictHeight: NSLayoutConstraint!
    @IBOutlet weak var viewMetricInputs: UIView!
    @IBOutlet weak var tfHeightMetric: UITextField!
    @IBOutlet weak var lbErrorHeightMetric: UILabel!
    @IBOutlet weak var lbWeightMetric: UILabel!
    @IBOutlet weak var tfWeightMetric: UITextField!
    @IBOutlet weak var lbErrorWeightMetric: UILabel!
    @IBOutlet weak var weightCmTag: UILabel!
    @IBOutlet weak var heightCmTag: UILabel!
    
    // IMPERIAL
    @IBOutlet weak var viewImperialInputs: UIView!
    @IBOutlet weak var ctImperialHeight: NSLayoutConstraint!
    @IBOutlet weak var lbHeightFeet: UILabel!
    @IBOutlet weak var tfHeightFeet: UITextField!
    @IBOutlet weak var lbErrorFeet: UILabel!
    @IBOutlet weak var tfHeightInches: UITextField!
    @IBOutlet weak var lbErrorInches: UILabel!
    @IBOutlet weak var heightFtTag: UILabel!
    @IBOutlet weak var heightInTag: UILabel!
    @IBOutlet weak var lbWeightImperial: UILabel!
    @IBOutlet weak var tfWeightImperial: UITextField!
    @IBOutlet weak var lbErrorWeightImperial: UILabel!
    @IBOutlet weak var weightInTag: UILabel!
    
    // AGE
    @IBOutlet weak var viewAgeHolder: UIView!
    @IBOutlet weak var lbAge: UILabel!
    @IBOutlet weak var lbErrorAge: UILabel!
    @IBOutlet weak var ageTag: UILabel!
    @IBOutlet weak var tfAge: UITextField!
    
    
    // FIT SLIDER VIEW HOLD
    @IBOutlet weak var viewFitHolder: UIView!
    @IBOutlet weak var lbFitControl: UILabel!
    @IBOutlet weak var sliderFitControl: UISlider!
    @IBOutlet weak var btnTighter: UIButton!
    @IBOutlet weak var btnLooser: UIButton!
    
    // NAV BUTTON
    @IBOutlet weak var btnNavToAdvanced: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    
    // SEGMENTED BUTTON GENRE
    @IBOutlet weak var sbGenre: UISegmentedControl!
    @IBOutlet weak var lbSegmentedBtnGenre: UILabel!
    @IBOutlet weak var ctSegmentedBtnGenreH: NSLayoutConstraint!
    @IBOutlet weak var ctLabelGenreH: NSLayoutConstraint!
    
    @IBOutlet weak var ctContentViewHeight: NSLayoutConstraint!
    
    // KEYBOARD TOOLBAR
    private var kbToolbar: UIToolbar?
    
    // MARK: - Constraints
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbInfoConstraint: NSLayoutConstraint!
    
    // AGE
    @IBOutlet weak var viewAgeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbAgeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tfAgeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbErrorAgeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ageTagHeightConstraint: NSLayoutConstraint!
    
    
    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    
    // MARK: - Private vars
    private var measuresList: [String]?
    private var limitsMetric: LimitsMetric?
    private var limitsImperial: LimitsImperial?
    private var subjectDataMetric: SubjectDataMetric?
    private var subjectDataImperial: SubjectDataImperial?
    private var selectedSex: String?
    private var imageProductURL: String?
    private var isImperial: Bool = false
    private var isOnlyHeight: Bool = false
    private var segmentedGenreKeys: [String] = []
    private var segmentedGenreValues: [String] = []
    
    // MARK: - Private values variables
    private var minInchesHeight: Float = 0
    private var maxInchesHeight: Float = 0
    private var currentInchesHeight: Int = 0
    
    private var heightCm: Float = 0
    private var heightFeet: Float = 0
    private var heightInches: Float = 0
    private var heightTotalInches: Float = 0
    private var weightKg: Float = 0
    private var weightLbs: Float = 0
    private var age: Int = 0
    private var fitValue: Int = 0
    
    private var intPartFeet: Int = 0
    private var decPartFeet: Float = 0
    private var intPartIn: Int = 0
    private var decPartIn: Float = 0
    
    private var sexProfile: Profile?
    
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariables()
        initViews()
    }
    
    override func viewDidLayoutSubviews() {
        showMetricInputs(show: !isImperial)
        showImperialInputs(show: isImperial)
        showAgeInput(show: isOnlyHeight)
        hideWeightMetric(hide: isOnlyHeight)
        hideWeightImperial(hide: isOnlyHeight)
        btnNavToAdvanced.isHidden = isOnlyHeight
        
        if popupData?.sex != SexValues.UNISEX.rawValue && popupData?.sex != SexValues.HUMAN.rawValue {
            hideGenreSegmentedBtn(hide: true)
        }
    }
    
    // MARK: - Views and variables Initializers
    private func initVariables() {
        measuresList = popupData?.measurements ?? []
        limitsImperial = popupData?.sex_limits?.imperial!
        limitsMetric = popupData?.sex_limits?.metric!
        imageProductURL = popupData?.image ?? nil
        initSelectedSex()
        updateSubjectData()
        initMeasuresValues()
        minInchesHeight = (limitsImperial?.height?.min?.ft)! * 12 + (limitsImperial?.height?.min?.inX)!
        maxInchesHeight = (limitsImperial?.height?.max?.ft)! * 12 + (limitsImperial?.height?.max?.inX)!
        isOnlyHeight = popupData?.only_height ?? false
        isImperial = SharedBetweenFormsData.shared.isImperial
    }
    
    private func initSelectedSex() {
        if popupData?.sex == SexValues.UNISEX.rawValue {
            let optionMale = localizedStringBundle(key: "labels.male", for: self.classForCoder).uppercased()
            let optionFemale = localizedStringBundle(key: "labels.female", for: self.classForCoder).uppercased()
            
            appendKeyValue(key: optionMale, value: SexValues.MALE.rawValue)
            appendKeyValue(key: optionFemale, value: SexValues.FEMALE.rawValue)
            
            selectedSex = SharedBetweenFormsData.shared.selectedSex ?? SexValues.MALE.rawValue
            SharedBetweenFormsData.shared.selectedSex = selectedSex
        } else if popupData?.sex == SexValues.HUMAN.rawValue {
            let optionAdult = localizedStringBundle(key: "labels.adult", for: self.classForCoder).uppercased()
            let optionChild = localizedStringBundle(key: "labels.child", for: self.classForCoder).uppercased()
            
            appendKeyValue(key: optionChild, value: SexValues.BOY.rawValue)
            appendKeyValue(key: optionAdult, value: SexValues.MALE.rawValue)
            
            selectedSex = SharedBetweenFormsData.shared.selectedSex ?? SexValues.BOY.rawValue
            SharedBetweenFormsData.shared.selectedSex = selectedSex
        } else {
            selectedSex = nil
            SharedBetweenFormsData.shared.selectedSex = nil
        }

        sexProfile = SharedBetweenFormsData.shared.getProfile(for: popupData?.sex)

    }
    
    private func appendKeyValue(key: String, value: String) {
        segmentedGenreKeys.append(key)
        segmentedGenreValues.append(value)
    }
    
    private func setupSegmentedGenreButton(keys: [String]) {
        lbSegmentedBtnGenre.text = localizedStringBundle(key: "title.genre", for: self.classForCoder)
        sbGenre.setTitle(keys[0], forSegmentAt: 0)
        sbGenre.setTitle(keys[1], forSegmentAt: 1)
        
        if let index = segmentedGenreValues.firstIndex(of: selectedSex ?? "") {
            sbGenre.selectedSegmentIndex = index
        } else {
            sbGenre.selectedSegmentIndex = 0
        }
        
        sbGenre.layer.cornerRadius = 4.0
        sbGenre.backgroundColor = UIColor.white
        sbGenre.tintColor = UIColor.colorPrimary
        
        if #available(iOS 13.0, *) {
            sbGenre.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            sbGenre.selectedSegmentTintColor = UIColor.colorPrimary
        }
        
        sbGenre.addTarget(self,  action: #selector(changeOption(sender:)), for: .valueChanged)
    }
    
    // MARK: - #selector
    @objc func changeOption(sender: UISegmentedControl) {
        selectedSex = segmentedGenreValues[sender.selectedSegmentIndex]
        
        SharedBetweenFormsData.shared.selectedSex = selectedSex
        
        sexProfile = SharedBetweenFormsData.shared.getProfile(for: popupData?.sex)
        
        // Refresh data on selected sex
        updateDataAndViewsOnSexChange()
    }
    
    private func updateSubjectData() {
        let dataMetric: DataMetric = (popupData?.data?.metric!)!
        let dataImperial: DataImperial = (popupData?.data?.imperial!)!
        let sex: String = (popupData?.sex)!
    
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(dataMetric: dataMetric, sex: sex, selectedSex: selectedSex)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(dataImperial: dataImperial, sex: sex, selectedSex: selectedSex)
    }
    
    private func initMeasuresValues() {
        heightCm = sexProfile?.metricHeight ?? subjectDataMetric?.height ?? 0
        heightFeet = sexProfile?.imperialHeight?.ft ?? subjectDataImperial?.height!.ft ?? 0
        heightInches = sexProfile?.imperialHeight?.inX ?? subjectDataImperial?.height!.inX ?? 0

        heightTotalInches = heightFeet * 12 + heightInches
        currentInchesHeight = Int(heightTotalInches)
        weightKg = sexProfile?.weight.metric ?? subjectDataMetric?.weight ?? 0
        weightLbs = sexProfile?.weight.imperial ??  subjectDataImperial?.weight! ?? 0
        age = sexProfile?.age ?? (isImperial ? subjectDataImperial?.age : subjectDataMetric?.age) ?? 0
        
        fitValue = sexProfile?.fit ?? subjectDataMetric?.fit ?? 2
        
        persistSharedData()
    }
    
    private func initViews() {
        tvTitle.text = localizedStringBundle(key: "title.basic", for: self.classForCoder)
        swMetricSystem.isOn = SharedBetweenFormsData.shared.isImperial //isImperial
        tvTitle.isEditable = false

        btnNavToAdvanced.titleLabel?.lineBreakMode = .byWordWrapping
        btnNavToAdvanced.titleLabel?.numberOfLines = 2
        btnNavToAdvanced.underlineButton(text: localizedStringBundle(key: "buttons.advanced", for: self.classForCoder))
        
        btnContinue.setTitle(localizedStringBundle(key: "buttons.continue", for: self.classForCoder).uppercased(), for: .normal)
        setUpKeyboardToolbar()
        setUpToolbarForInputs()
        initMetricInputs()
        initImperialInputs()
        initAgeInput()
        initSliderControl()
        setDefaultFormValues()
        initImageView()
        
        if selectedSex != nil {
            setupSegmentedGenreButton(keys: segmentedGenreKeys)
        }
    }
    
    private func setUpKeyboardToolbar() {
        kbToolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(kbDoneButtonTapped))
        kbToolbar?.setItems([flexSpace, doneButton], animated: true)
        kbToolbar?.sizeToFit()
    }
    
    @objc func kbDoneButtonTapped() {
        view.endEditing(true)
    }
    
    private func setUpToolbarForInputs() {
        tfHeightMetric.inputAccessoryView = kbToolbar
        tfHeightFeet.inputAccessoryView = kbToolbar
        tfHeightInches.inputAccessoryView = kbToolbar
        tfWeightMetric.inputAccessoryView = kbToolbar
        tfWeightImperial.inputAccessoryView = kbToolbar
        tfAge.inputAccessoryView = kbToolbar
    }
    
    private func initImageView() {
        if !isNilOrBlank(str: imageProductURL) {
            requestImage(url: imageProductURL!) { image in
                self.ivProduct.image = image
            }
        } else {
            ivProduct.isHidden = true
            imageWidthConstraint.constant = 0
            ivProduct.clipsToBounds = true
            lbInfoConstraint.constant = 60
        }
    }
    private func initMetricInputs() {
        lbHeightMetric.text = localizedStringBundle(key: "measurements.height", for: self.classForCoder)
        lbWeightMetric.text = localizedStringBundle(key: "measurements.weight", for: self.classForCoder)
        lbErrorHeightMetric.text = ""
        lbErrorWeightMetric.text = ""
        tfHeightMetric.keyboardType = .numberPad
        tfWeightMetric.keyboardType = .numberPad
    }
    private func initAgeInput() {
        lbAge.text = localizedStringBundle(key: "labels.age", for: self.classForCoder)
        ageTag.text = localizedStringBundle(key: "labels.years", for: self.classForCoder)
        lbErrorAge.text = ""
        tfAge.keyboardType = .numberPad
    }
    private func setDefaultFormValues() {
        // Metric
        tfHeightMetric.text = String(Int(heightCm))
        tfWeightMetric.text = String(Int(weightKg))
        
        // Imperial
        tfHeightFeet.text = String(Int(heightFeet))
        tfHeightInches.text = String(Int(heightInches))
        tfWeightImperial.text = String(Int(weightLbs))
        
        // Age
        tfAge.text = String(age)
        
    }
    private func initImperialInputs() {
        lbHeightFeet.text = localizedStringBundle(key: "labels.feet", for: self.classForCoder)
        lbErrorFeet.text = ""
        lbErrorInches.text = ""
        lbWeightImperial.text = localizedStringBundle(key: "measurements.weight", for: self.classForCoder)
        lbErrorWeightImperial.text = ""
        tfHeightFeet.keyboardType = .numberPad
        tfHeightInches.keyboardType = .numberPad
        tfWeightImperial.keyboardType = .numberPad
    }
    private func initSliderControl() {
        lbFitControl.text = localizedStringBundle(key: "title.fitting", for: self.classForCoder)
        btnTighter.setTitle("< " + localizedStringBundle(key: "fitting.1", for: self.classForCoder).capitalized, for: .normal)
        btnLooser.setTitle(localizedStringBundle(key: "fitting.3", for: self.classForCoder).capitalized + " >", for: .normal)
        sliderFitControl.value = Float(fitValue) //sexProfile?.fit ?? Float(subjectDataMetric?.fit ?? 2)
    }
    
    // MARK: - IBAction
    @IBAction func onSystemMetricChange(_ sender: UISwitch) {
        isImperial = swMetricSystem.isOn
        SharedBetweenFormsData.shared.isImperial = isImperial
        updateInputsAndValuesOnSystemMetricChange()
        switchForm()
    }
    @IBAction func onFitSliderChange(_ sender: UISlider) {
        let step: Float = SliderFitValues.STEP.rawValue
        let currentValue = round((sender.value - sender.minimumValue) / step)
        sender.value = currentValue
        btnTighter.isEnabled = currentValue > SliderFitValues.MIN.rawValue
        btnLooser.isEnabled = currentValue < SliderFitValues.MAX.rawValue
        fitValue = Int(currentValue)
        sexProfile?.fit = fitValue
    }
    // BUTTONS
    @IBAction func onBtnTightPress(_ sender: UIButton) {
        if (sliderFitControl.value < SliderFitValues.MIN.rawValue) {
            sliderFitControl.value = SliderFitValues.MIN.rawValue
        } else {
            sliderFitControl.value -= 1.0
        }
    }
    @IBAction func onBtnLooserPress(_ sender: UIButton) {
        if (sliderFitControl.value > SliderFitValues.MAX.rawValue) {
            sliderFitControl.value = SliderFitValues.MAX.rawValue
        } else {
            sliderFitControl.value += 1.0
        }
    }
    
    @IBAction func onBtnNavToAdvancedPress(_ sender: UIButton) {
        persistSharedData()
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let measuresForm = storyboard.instantiateViewController(withIdentifier: "YourMeasuresFormViewController") as! YourMeasuresFormViewController
        measuresForm.contentView = contentView
        measuresForm.popupData = popupData
        measuresForm.product = self.product
        contentView!.show(from: self, to: measuresForm)
    }
    
    @IBAction func onBtnCountinuePress(_ sender: UIButton) {
        var isValidForm: Bool = true
        
        if (isImperial) {
            // check y set errores
            scanAllImperialInputs()
            // validate imperial fields
            isValidForm = !FormValidationHelper.someHasError(labels: lbErrorFeet, lbErrorInches, lbErrorWeightImperial, lbErrorAge)
        } else {
            // check y set errores
            scanAllMetricInputs()
            // validate metric fields
            isValidForm = !FormValidationHelper.someHasError(labels: lbErrorHeightMetric, lbErrorWeightMetric, lbErrorAge)
        }
        
        if isValidForm {
            persistSharedData()
            if isOnlyHeight {
                navigateToRecommend()
            } else {
                SharedBetweenFormsData.shared.isFromAdvanced = false
                navigateToLastStep()
            }
        }
    }
    
    private func persistSharedData() {
        let _weight = SharedBetweenFormsData.Measure(metric: weightKg, imperial: weightLbs)
        
        sexProfile?.metricHeight = heightCm
        sexProfile?.imperialHeight = HeightImperial(ft: heightFeet, inX: heightInches)
        sexProfile?.weight = _weight
        sexProfile?.fit = fitValue
        sexProfile?.age = age
    }
    
    // MARK: - IBActions TextFields Metric
    @IBAction func onHeightMetricDidEdit(_ sender: UITextField) {
        updateHeightMetricInput(sender)
    }
    @IBAction func onHeightMetricChange(_ sender: UITextField) {
        updateHeightMetricInput(sender)
    }
    
    @IBAction func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            textField.selectAll(nil)
        }
    }
    
    @IBAction func onWeightMetricDidEdit(_ sender: UITextField) {
        updateWeightInput(sender)
    }
    @IBAction func onWeightMetricChange(_ sender: UITextField) {
        updateWeightInput(sender)
    }
    
    private func updateHeightMetricInput(_ sender: UITextField) {
        var heightAux: Any = heightCm
        FormValidationHelper.setInputError(text: sender.text!,measureLimit: (limitsMetric?.height!)!, errorLabel: lbErrorHeightMetric, value: &heightAux)
        heightCm = heightAux as! Float
        sexProfile?.metricHeight = heightCm
    }
    
    private func updateWeightInput(_ sender: UITextField) {
        if (SharedBetweenFormsData.shared.isImperial) {
            var weightLbsAux: Any = weightLbs
            FormValidationHelper.setInputError(text: sender.text!, measureLimit: (limitsImperial?.weight!)!, errorLabel: lbErrorWeightImperial, value: &weightLbsAux)
            weightLbs = weightLbsAux as! Float
            sexProfile?.weight.imperial = weightLbs
        } else {
            var weightAux: Any = weightKg
            FormValidationHelper.setInputError(text: sender.text!,measureLimit: (limitsMetric?.weight!)!, errorLabel: lbErrorWeightMetric, value: &weightAux)
            weightKg = weightAux as! Float
            sexProfile?.weight.metric = weightKg
        }
        
    }
    
    // MARK: - IBActions TextFields Imperial
    @IBAction func onFeetImperialDidEdit(_ sender: UITextField) {
        updateFeetInput(sender)
    }
    @IBAction func onFeetImperialChange(_ sender: UITextField) {
        updateFeetInput(sender)
    }
    
    private func updateFeetInput(_ sender: UITextField) {
        _ = manageHeightFeetErrorMessage(text: sender.text)
        
        if !isNilOrBlank(str: sender.text) {
            let inputValue = (sender.text! as NSString).floatValue
            intPartFeet = Int(inputValue)
            heightFeet = Float(intPartFeet) + decPartFeet
            sexProfile?.imperialHeight?.ft = heightFeet
        }
    }
    
    @IBAction func onInchesImperialDidEdit(_ sender: UITextField) {
        updateInchesImperial(sender)
    }
    @IBAction func onInchesImperialChange(_ sender: UITextField) {
        updateInchesImperial(sender)
    }
    
    private func updateInchesImperial(_ sender: UITextField) {
        _ = manageHeightInchesErrorMessage(text: sender.text)
        
        if !isNilOrBlank(str: sender.text) {
            let inputValue = (sender.text! as NSString).floatValue
            intPartIn = Int(inputValue)
            heightInches = Float(intPartIn) + decPartIn
            sexProfile?.imperialHeight?.inX = heightInches
        }
    }
    
    @IBAction func onWeightImperialDidEdit(_ sender: UITextField) {
        updateWeightInput(sender)
    }
    @IBAction func onWeightImperialChange(_ sender: UITextField) {
        updateWeightInput(sender)
    }
    
    // MARK: - IBActions Age Input
    @IBAction func onAgeDidEdit(_ sender: UITextField) {
        updateAgeInput(sender)
    }
    @IBAction func onAgeChange(_ sender: UITextField) {
        updateAgeInput(sender)
    }
    
    private func updateAgeInput(_ sender: UITextField) {
        let value = sender.text!
        let min: Float = isImperial ? (limitsImperial?.age?.min!)! : (limitsMetric?.age?.min!)!
        let max: Float = isImperial ? (limitsImperial?.age?.max!)! : (limitsMetric?.age?.max!)!
        
        if FormValidationHelper.isOutOfValidRange(value: value, min: min, max: max) {
            lbErrorAge.text = "\(Int(min)) - \(Int(max))"
        } else {
            lbErrorAge.text = ""
            age = Int(value) ?? 0
        }
    }
    
    
    // MARK: - Render Helpers
    private func switchForm() {
        showMetricInputs(show: !isImperial)
        showImperialInputs(show: isImperial)
    }
    
    private func showMetricInputs(show: Bool) {
        viewMetricInputs.isHidden = !show
    }
    
    private func showImperialInputs(show: Bool) {
        viewImperialInputs.isHidden = !show
    }
    
    private func showAgeInput(show: Bool) {
        viewAgeHolder.isHidden = !show
        viewAgeHolder.clipsToBounds = true
        viewAgeHeightConstraint.constant = !show ? 0 : 92
    }
    
    // MARK: - Inputs group visibility methods
    private func hideAllInputs() {
        hideHeightMetric(hide: true)
        hideWeightMetric(hide: true)
        hideHeightImperial(hide: true)
        hideWeightImperial(hide: true)
    }
    
    // METRIC
    private func hideHeightMetric(hide: Bool) {
        
        if (hide && !lbHeightMetric.isHidden){
            ctMetrictHeight.constant -= INPUT_HEIGHT
        }else if  (!hide && lbHeightMetric.isHidden){
            ctMetrictHeight.constant += INPUT_HEIGHT
        }
        
        lbHeightMetric.isHidden = hide
        lbErrorHeightMetric.isHidden = hide
        tfHeightMetric.isHidden = hide
        heightCmTag.isHidden = hide
    }
    
    private func hideWeightMetric(hide: Bool) {
        if (hide && !lbWeightMetric.isHidden){
            ctMetrictHeight.constant -= INPUT_HEIGHT
        }else if (!hide && lbWeightMetric.isHidden){
            ctMetrictHeight.constant += INPUT_HEIGHT
        }
        
        lbWeightMetric.isHidden = hide
        lbErrorWeightMetric.isHidden = hide
        tfWeightMetric.isHidden = hide
        weightCmTag.isHidden = hide
    }
    
    // IMPERIAL
    private func hideHeightImperial(hide: Bool) {
        if (hide && !lbHeightFeet.isHidden){
            ctImperialHeight.constant -= INPUT_HEIGHT
        }else if  (!hide && lbHeightFeet.isHidden){
            ctImperialHeight.constant += INPUT_HEIGHT
        }
        
        lbHeightFeet.isHidden = hide
        lbErrorFeet.isHidden = hide
        lbErrorInches.isHidden = hide
        tfHeightFeet.isHidden = hide
        tfHeightInches.isHidden = hide
        heightFtTag.isHidden = hide
        heightInTag.isHidden = hide
    }
    private func hideWeightImperial(hide: Bool) {
        if (hide && !lbWeightImperial.isHidden){
            ctImperialHeight.constant -= INPUT_HEIGHT
        }else if  (!hide && lbWeightImperial.isHidden){
            ctImperialHeight.constant += INPUT_HEIGHT
        }
        
        lbWeightImperial.isHidden = hide
        lbErrorWeightImperial.isHidden = hide
        tfWeightImperial.isHidden = hide
        weightInTag.isHidden = hide
    }
    
    private func hideGenreSegmentedBtn(hide: Bool) {
        lbSegmentedBtnGenre.isHidden = hide
        sbGenre.isHidden = hide
        ctLabelGenreH.constant = 0
        ctSegmentedBtnGenreH.constant = 0
    }
    
    // MARK: - Self Utils Helpers
    private func manageHeightInchesErrorMessage(text: String?) -> Bool {
        let inches = Int(text ?? "0") ?? 03
        setCurrentInches(feet: tfHeightFeet.text ?? "0", inches: String(inches))
        var hasError = false
        let isTextEmptyOrNil = text!.isEmpty || text == nil
        
        if (isTextEmptyOrNil || currentInchesHeight < Int(minInchesHeight) || currentInchesHeight > Int(maxInchesHeight) || inches > 11) {
            let minFt = Int((limitsImperial?.height?.min?.ft)!)
            let maxFt = Int((limitsImperial?.height?.max?.ft)!)
            let minInx = Int((limitsImperial?.height?.min?.inX)!)
            let maxInx = Int((limitsImperial?.height?.max?.inX)!)
            lbErrorFeet.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            lbErrorInches.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            hasError = true
        } else {
            lbErrorFeet.text = ""
            lbErrorInches.text = ""
        }
        return hasError
    }
    
    private func manageHeightFeetErrorMessage(text: String?) -> Bool {
        setCurrentInches(feet: text ?? "0", inches: tfHeightInches.text ?? "0")
        var hasError = false
        let isTextEmptyOrNil = text!.isEmpty || text == nil
        if (isTextEmptyOrNil || currentInchesHeight < Int(minInchesHeight) || currentInchesHeight > Int(maxInchesHeight)) {
            let minFt = Int((limitsImperial?.height?.min?.ft)!)
            let maxFt = Int((limitsImperial?.height?.max?.ft)!)
            let minInx = Int((limitsImperial?.height?.min?.inX)!)
            let maxInx = Int((limitsImperial?.height?.max?.inX)!)
            lbErrorFeet.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            lbErrorInches.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            hasError = true
        } else {
            lbErrorFeet.text = ""
            lbErrorInches.text = ""
        }
        return hasError
    }
    
    private func setCurrentInches(feet: String, inches: String) {
        let heightFeet = Int(feet) ?? 0
        let feetInches: Int = heightFeet * 12
        let inches: Int = Int(inches) ?? 0
        currentInchesHeight = feetInches + inches
    }
    
    private func updateInputsAndValuesOnSystemMetricChange() {
        var metricSystem = MetricSystem.METRIC
        if isImperial {
            metricSystem = MetricSystem.IMPERIAL
        }
        updateHeightInputsAndValues(type: metricSystem)
        updateInputsAndValues(type: metricSystem)
    }
    private func updateHeightInputsAndValues(type: MetricSystem) {
        switch type {
        case MetricSystem.METRIC:
            heightTotalInches = MetricsConvertHelpers.getTotalInchesFromImperial(heightImperial: HeightImperial(ft: heightFeet, inX: heightInches))
            heightCm = MetricsConvertHelpers.fromFeetInchesToCm(feet: heightFeet, inches: heightInches)
            tfHeightMetric.text = String(Int(floor(heightCm)))
            
        case MetricSystem.IMPERIAL:
            let inputValue: String? = tfHeightMetric.text
            if (!isNilOrBlank(str: inputValue)) {
                heightCm = (inputValue! as NSString).floatValue
            }
            heightTotalInches = MetricsConvertHelpers.getTotalInchesFromCm(cm: heightCm)
            let imperialHeight: HeightImperial = MetricsConvertHelpers.getImperialFromTotalInches(totalInches: heightTotalInches)
            heightFeet = imperialHeight.ft ?? 0
            heightInches = imperialHeight.inX ?? 0
            
            tfHeightFeet.text = String(Int(floor(heightFeet)))
            tfHeightInches.text = String(Int(floor(heightInches)))
        }
        
    }
    private func updateInputsAndValues(type: MetricSystem) {
        switch type {
        case MetricSystem.METRIC:
            let inputValue: String? = tfWeightImperial.text
            if !isNilOrBlank(str: inputValue) {
                let valueNum: Float = (inputValue! as NSString).floatValue
                let decPart: Float = MetricsConvertHelpers.getDecimalPart(num: weightLbs)
                weightLbs = valueNum + decPart
            }
            weightKg = MetricsConvertHelpers.fromPoundsToKgs(pounds: weightLbs)
            tfWeightMetric.text = String(Int(floor(weightKg)))
            
        case MetricSystem.IMPERIAL:
            let inputValue: String? = tfWeightMetric.text
            if !isNilOrBlank(str: inputValue) {
                let valueNum: Float = (inputValue! as NSString).floatValue
                let decPart: Float = MetricsConvertHelpers.getDecimalPart(num: weightKg)
                weightKg = valueNum + decPart
            }
            weightLbs = MetricsConvertHelpers.fromKgToPounds(kgs: weightKg)
            tfWeightImperial.text = String(Int(floor(weightLbs)))
        }
    }
    
    private func getSaveRecommendRequest() -> SaveRecommendRequest{
        let request = SaveRecommendRequest(product: product)
        
        request.ck = popupData?.ck
        request.sex = selectedSex ?? popupData?.sex
        request.fit = fitValue
        request.w = RecommendationWay.BASIC_WAY.rawValue        
        if isOnlyHeight {
            request.age = age
        }
        
        if (!isImperial){
            request.weight = weightKg > 0 ? String(weightKg) : String((tfWeightMetric.text! as NSString).floatValue)
            request.height = heightCm > 0 ? String(heightCm) : String((tfHeightMetric.text! as NSString).floatValue)
            request.metric_system = MetricSystem.METRIC.rawValue
            request.mass_system = MetricSystem.METRIC.rawValue
        } else {
            request.weight = weightLbs > 0 ? String(weightLbs) : String((tfWeightImperial.text! as NSString).floatValue)
            request.height = heightTotalInches > 0 ? String(heightTotalInches) : String((tfHeightFeet.text! as NSString).floatValue * 12 + (tfHeightInches.text! as NSString).floatValue)
            request.metric_system = MetricSystem.IMPERIAL.rawValue
            request.mass_system = MetricSystem.IMPERIAL.rawValue
        }
        return request
    }
    
    private func navigateToRecommend() {
        let request = getSaveRecommendRequest()
        
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let resultsForm = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        
        resultsForm.contentView = contentView
        resultsForm.popupData = popupData
        resultsForm.product = self.product
        resultsForm.request = request
        contentView!.show(from: self, to: resultsForm)
    }
    
    private func  navigateToLastStep() {
        let request = getSaveRecommendRequest()
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let lastStep = storyboard.instantiateViewController(withIdentifier: "LastStepViewController") as! LastStepViewController
        
        lastStep.contentView = contentView
        lastStep.popupData = popupData
        lastStep.product = self.product
        lastStep.request = request
        
        contentView!.show(from: self, to: lastStep)
    }
    
    private func scanAllMetricInputs() {
        // Height
        if limitsMetric?.height != nil {
            var heightAux: Any = heightCm
            FormValidationHelper.setInputError(text: tfHeightMetric.text!,measureLimit: (limitsMetric?.height!)!, errorLabel: lbErrorHeightMetric, value: &heightAux)
            heightCm = heightAux as! Float
        }
        // weight
        if limitsMetric?.weight != nil {
            var weightAux: Any = weightKg
            FormValidationHelper.setInputError(text: tfWeightMetric.text!,measureLimit: (limitsMetric?.weight!)!, errorLabel: lbErrorWeightMetric, value: &weightAux)
            weightKg = weightAux as! Float
        }
        // age
        if !viewAgeHolder.isHidden && limitsMetric?.age != nil {
            let min: Float = isImperial ? (limitsImperial?.age?.min!)! : (limitsMetric?.age?.min!)!
            let max: Float = isImperial ? (limitsImperial?.age?.max!)! : (limitsMetric?.age?.max!)!
            
            if FormValidationHelper.isOutOfValidRange(value: tfAge.text, min: min, max: max) {
                lbErrorAge.text = "\(Int(min)) - \(Int(max))"
            } else {
                lbErrorAge.text = ""
                age = Int((tfAge?.text)!) ?? 0
            }
        }
    }
    
    private func scanAllImperialInputs() {
        // feet
        _ = manageHeightFeetErrorMessage(text: tfHeightFeet.text)
        // inches
        _ = manageHeightInchesErrorMessage(text: tfHeightInches.text)
        // weight
        if limitsImperial?.weight != nil {
            var weightLbsAux: Any = weightLbs
            FormValidationHelper.setInputError(text: tfWeightImperial.text!, measureLimit: (limitsImperial?.weight!)!, errorLabel: lbErrorWeightImperial, value: &weightLbsAux)
            weightLbs = weightLbsAux as! Float
        }
        // age
        if limitsMetric?.age != nil {
            let min: Float = isImperial ? (limitsImperial?.age?.min!)! : (limitsMetric?.age?.min!)!
            let max: Float = isImperial ? (limitsImperial?.age?.max!)! : (limitsMetric?.age?.max!)!
            
            if FormValidationHelper.isOutOfValidRange(value: tfAge.text, min: min, max: max) {
                lbErrorAge.text = "\(Int(min)) - \(Int(max))"
            } else {
                lbErrorAge.text = ""
                age = Int((tfAge?.text)!) ?? 0
            }
        }
    }
    
    private func updateDataAndViewsOnSexChange() {
        updateSubjectData()
        initMeasuresValues()
        setDefaultFormValues()
        initSliderControl()
    }
 }
