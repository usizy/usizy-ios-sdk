//
//  YourMeasuresFormViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 01/03/2021.
//

import UIKit

class YourMeasuresFormViewController: UIViewController {
    private let INPUT_HEIGHT = CGFloat(120)
    
    // MARK: - IBOulets
    @IBOutlet weak var ivProduct: UIImageView!
    @IBOutlet weak var tvTitle: UITextView!
    @IBOutlet weak var swMetricSystem: UISwitch!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbInfoConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeightContraint: NSLayoutConstraint!
    
    // METRIC
    @IBOutlet weak var lbHeightMetric: UILabel!
    @IBOutlet weak var viewMetricInputs: UIView!
    @IBOutlet weak var tfHeightMetric: UITextField!
    @IBOutlet weak var lbErrorHeightMetric: UILabel!
    @IBOutlet weak var lbChestMetric: UILabel!
    @IBOutlet weak var tfChestMetric: UITextField!
    @IBOutlet weak var lbErrorChestMetric: UILabel!
    @IBOutlet weak var lbWaistMetric: UILabel!
    @IBOutlet weak var tfWaistMetric: UITextField!
    @IBOutlet weak var lbErrorWaistMetric: UILabel!
    @IBOutlet weak var lbHipsMetric: UILabel!
    @IBOutlet weak var tfHipsMetric: UITextField!
    @IBOutlet weak var lbErrorHipsMetric: UILabel!
    @IBOutlet weak var heightCmTag: UILabel!
    @IBOutlet weak var chestCmTag: UILabel!
    @IBOutlet weak var waistCmTag: UILabel!
    @IBOutlet weak var hipsCmTag: UILabel!
    @IBOutlet weak var heightMetricViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var chestMetricViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var waistMetricViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var hipsMetricViewHeightConstraint: NSLayoutConstraint!
    
    // IMPERIAL
    @IBOutlet weak var viewImperialInputs: UIView!
    @IBOutlet weak var lbHeightFeet: UILabel!
    @IBOutlet weak var tfHeightFeet: UITextField!
    @IBOutlet weak var lbErrorFeet: UILabel!
    @IBOutlet weak var tfHeightInches: UITextField!
    @IBOutlet weak var lbErrorInches: UILabel!
    @IBOutlet weak var lbChestImperial: UILabel!
    @IBOutlet weak var tfChestImperial: UITextField!
    @IBOutlet weak var lbErrorChestImperial: UILabel!
    @IBOutlet weak var lbWaistImperial: UILabel!
    @IBOutlet weak var tfWaistImperial: UITextField!
    @IBOutlet weak var lbErrorWaistImperial: UILabel!
    @IBOutlet weak var lbHipsImperial: UILabel!
    @IBOutlet weak var tfHipsImperial: UITextField!
    @IBOutlet weak var lbErrorHipsImperial: UILabel!
    @IBOutlet weak var heightFtTag: UILabel!
    @IBOutlet weak var heightInTag: UILabel!
    @IBOutlet weak var chestInTag: UILabel!
    @IBOutlet weak var waistInTag: UILabel!
    @IBOutlet weak var hipsInTag: UILabel!
    @IBOutlet weak var heightImperialViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var chestImperialViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var waistImperialViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var hipsImperialViewHeightConstraint: NSLayoutConstraint!
    
    // FIT SLIDER VIEW HOLD
    @IBOutlet weak var viewFitHolder: UIView!
    @IBOutlet weak var lbFitControl: UILabel!
    @IBOutlet weak var sliderFitControl: UISlider!
    @IBOutlet weak var btnTighter: UIButton!
    @IBOutlet weak var btnLooser: UIButton!
    
    // NAV BUTTON
    @IBOutlet weak var btnNavToBasic: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    
    // SEGMENTED BUTTON
    @IBOutlet weak var lbGenreAdvance: UILabel!
    @IBOutlet weak var sbGenreAdv: UISegmentedControl!
    @IBOutlet weak var ctSbGenreAdv: NSLayoutConstraint!
    @IBOutlet weak var ctLbGenreAdv: NSLayoutConstraint!
    
    
    // KEYBOARD TOOLBAR
    private var kbToolbar: UIToolbar?
    
    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    
    // MARK: - Private vars
    private var measuresList: [String]?
    private var limitsMetric: LimitsMetric?
    private var limitsImperial: LimitsImperial?
    private var subjectDataMetric: SubjectDataMetric?
    private var subjectDataImperial: SubjectDataImperial?
    private var selectedSex: String?
    private var imageProductURL: String?
    private var isImperial: Bool = false
    private var segmentedGenreKeys: [String] = []
    private var segmentedGenreValues: [String] = []
    
    // MARK: - Private values variables
    private var minInchesHeight: Float = 0
    private var maxInchesHeight: Float = 0
    private var currentInchesHeight: Int = 0
    
    private var heightCm: Float = 0
    private var heightFeet: Float = 0
    private var heightInches: Float = 0
    private var heightTotalInches: Float = 0
    private var chestCm: Float = 0
    private var chestInches: Float = 0
    private var waistCm: Float = 0
    private var waistInches: Float = 0
    private var hipsCm: Float = 0
    private var hipsInches: Float = 0
    private var fitValue: Int = 0
    
    private var intPartFeet: Int = 0
    private var decPartFeet: Float = 0
    private var intPartIn: Int = 0
    private var decPartIn: Float = 0
    
    @IBOutlet weak var metricViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imperialViewHeightConstraint: NSLayoutConstraint!
    
    private var sexProfile: Profile?
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariables()
        initViews()
    }
    
    override func viewDidLayoutSubviews() {
        showMetricInputs(show: !isImperial)
        showImperialInputs(show: isImperial)
        renderIfMeasurement()
        if popupData?.sex != SexValues.UNISEX.rawValue && popupData?.sex != SexValues.HUMAN.rawValue {
            hideGenreSegmentedBtn(hide: true)
        }
    }
    
    // MARK: - Views and variables Initializers
    private func initVariables() {
        measuresList = popupData?.measurements ?? []
        limitsImperial = popupData?.sex_limits?.imperial!
        limitsMetric = popupData?.sex_limits?.metric!
        imageProductURL = popupData?.image ?? nil
        initSelectedSex()
        updateSubjectData()
        initMeasuresValues()
        minInchesHeight = (limitsImperial?.height?.min?.ft)! * 12 + (limitsImperial?.height?.min?.inX)!
        maxInchesHeight = (limitsImperial?.height?.max?.ft)! * 12 + (limitsImperial?.height?.max?.inX)!
        isImperial = SharedBetweenFormsData.shared.isImperial
    }
    
    private func initSelectedSex() {
        if popupData?.sex == SexValues.UNISEX.rawValue {
            let optionMale = localizedStringBundle(key: "labels.male", for: self.classForCoder).uppercased()
            let optionFemale = localizedStringBundle(key: "labels.female", for: self.classForCoder).uppercased()
            
            appendKeyValue(key: optionMale, value: SexValues.MALE.rawValue)
            appendKeyValue(key: optionFemale, value: SexValues.FEMALE.rawValue)
            
            selectedSex = SharedBetweenFormsData.shared.selectedSex ?? SexValues.MALE.rawValue
            SharedBetweenFormsData.shared.selectedSex = selectedSex
        } else if popupData?.sex == SexValues.HUMAN.rawValue {
            let optionAdult = localizedStringBundle(key: "labels.adult", for: self.classForCoder).uppercased()
            let optionChild = localizedStringBundle(key: "labels.child", for: self.classForCoder).uppercased()
            
            appendKeyValue(key: optionChild, value: SexValues.BOY.rawValue)
            appendKeyValue(key: optionAdult, value: SexValues.MALE.rawValue)
            
            selectedSex = SharedBetweenFormsData.shared.selectedSex ?? SexValues.BOY.rawValue
            SharedBetweenFormsData.shared.selectedSex = selectedSex
        } else {
            selectedSex = nil
            SharedBetweenFormsData.shared.selectedSex = nil
        }
        
        sexProfile = SharedBetweenFormsData.shared.getProfile(for: popupData?.sex)
    }
    
    private func appendKeyValue(key: String, value: String) {
        segmentedGenreKeys.append(key)
        segmentedGenreValues.append(value)
    }
    
    private func setupSegmentedGenreButton(keys: [String]) {
        lbGenreAdvance.text = localizedStringBundle(key: "title.genre", for: self.classForCoder)
        sbGenreAdv.setTitle(keys[0], forSegmentAt: 0)
        sbGenreAdv.setTitle(keys[1], forSegmentAt: 1)
        
        if let index = segmentedGenreValues.firstIndex(of: selectedSex ?? "") {
            sbGenreAdv.selectedSegmentIndex = index
        } else {
            sbGenreAdv.selectedSegmentIndex = 0
        }
        
        sbGenreAdv.layer.cornerRadius = 4.0
        sbGenreAdv.backgroundColor = UIColor.white
        sbGenreAdv.tintColor = UIColor.colorPrimary
        
        if #available(iOS 13.0, *) {
            sbGenreAdv.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            sbGenreAdv.selectedSegmentTintColor = UIColor.colorPrimary
        }
        
        sbGenreAdv.addTarget(self,  action: #selector(changeOption(sender:)), for: .valueChanged)
    }
    
    // MARK: - #selector
    @objc func changeOption(sender: UISegmentedControl) {
        selectedSex = segmentedGenreValues[sender.selectedSegmentIndex]
        
        SharedBetweenFormsData.shared.selectedSex = selectedSex
        
        sexProfile = SharedBetweenFormsData.shared.getProfile(for: popupData?.sex)
        // Refresh data on selected sex
        updateDataAndViewsOnSexChange()
    }
    
    private func updateSubjectData() {
        let dataMetric: DataMetric = (popupData?.data?.metric!)!
        let dataImperial: DataImperial = (popupData?.data?.imperial!)!
        let sex: String = (popupData?.sex)!
    
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(dataMetric: dataMetric, sex: sex, selectedSex: selectedSex)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(dataImperial: dataImperial, sex: sex, selectedSex: selectedSex)
    }
    
    private func initMeasuresValues() {
        heightCm = sexProfile?.metricHeight ?? subjectDataMetric?.height ?? 0
        heightFeet = sexProfile?.imperialHeight?.ft ?? subjectDataImperial?.height!.ft ?? 0
        heightInches = sexProfile?.imperialHeight?.inX ?? subjectDataImperial?.height!.inX ?? 0
        heightTotalInches = heightFeet * 12 + heightInches
        currentInchesHeight = Int(heightTotalInches)
        
        chestCm = sexProfile?.chest.metric ?? subjectDataMetric?.chest ?? 0
        chestInches =  sexProfile?.chest.imperial ?? subjectDataImperial?.chest! ?? 0
        waistCm =  sexProfile?.waist.metric ?? subjectDataMetric?.waist! ?? 0
        waistInches =  sexProfile?.waist.imperial ?? subjectDataImperial?.waist! ?? 0
        hipsCm =  sexProfile?.hips.metric ?? subjectDataMetric?.hips! ?? 0
        hipsInches = sexProfile?.hips.imperial ?? subjectDataImperial?.hips! ?? 0
        
        fitValue = sexProfile?.fit ?? subjectDataMetric?.fit ?? 2
        
        persistSharedData()
    }
    
    private func initViews() {
        tvTitle.text = localizedStringBundle(key: "title.basic", for: self.classForCoder)
        swMetricSystem.isOn = isImperial
        tvTitle.isEditable = false

        btnNavToBasic.titleLabel?.lineBreakMode = .byWordWrapping
        btnNavToBasic.titleLabel?.numberOfLines = 2
        btnNavToBasic.underlineButton(text: localizedStringBundle(key: "buttons.basic", for: self.classForCoder))
        
        btnContinue.setTitle(localizedStringBundle(key: "buttons.continue", for: self.classForCoder).uppercased(), for: .normal)
        setUpKeyboardToolbar()
        setUpToolbarForInputs()
        initMetricInputs()
        initImperialInputs()
        initSliderControl()
        setDefaultFormValues()
        initImageView()
        
        if selectedSex != nil {
            setupSegmentedGenreButton(keys: segmentedGenreKeys)
        }
    }
    
    private func setUpKeyboardToolbar() {
        kbToolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(kbDoneButtonTapped))
        kbToolbar?.setItems([flexSpace, doneButton], animated: true)
        kbToolbar?.sizeToFit()
    }
    
    @objc func kbDoneButtonTapped() {
        view.endEditing(true)
    }
    
    private func setUpToolbarForInputs() {
        tfHeightMetric.inputAccessoryView = kbToolbar
        tfHeightFeet.inputAccessoryView = kbToolbar
        tfHeightInches.inputAccessoryView = kbToolbar
        tfChestMetric.inputAccessoryView = kbToolbar
        tfChestImperial.inputAccessoryView = kbToolbar
        tfWaistMetric.inputAccessoryView = kbToolbar
        tfWaistImperial.inputAccessoryView = kbToolbar
        tfHipsMetric.inputAccessoryView = kbToolbar
        tfHipsImperial.inputAccessoryView = kbToolbar
    }
    
    private func initImageView() {
        if !isNilOrBlank(str: imageProductURL) {
            requestImage(url: imageProductURL!) { image in
                self.ivProduct.image = image
            }
        } else {
            ivProduct.isHidden = true
            imageWidthConstraint.constant = 0
            ivProduct.clipsToBounds = true
            lbInfoConstraint.constant = 60
        }
    }
    
    private func initMetricInputs() {
        lbHeightMetric.text = localizedStringBundle(key: "measurements.height", for: self.classForCoder)
        lbChestMetric.text = localizedStringBundle(key: "measurements.chest", for: self.classForCoder)
        lbWaistMetric.text = localizedStringBundle(key: "measurements.waist", for: self.classForCoder)
        lbHipsMetric.text = localizedStringBundle(key: "measurements.hips", for: self.classForCoder)
        lbErrorHeightMetric.text = ""
        lbErrorChestMetric.text = ""
        lbErrorWaistMetric.text = ""
        lbErrorHipsMetric.text = ""
        tfHipsMetric.keyboardType = .numberPad
        tfChestMetric.keyboardType = .numberPad
        tfWaistMetric.keyboardType = .numberPad
        tfHipsMetric.keyboardType = .numberPad
    }
    
    private func setDefaultFormValues() {
        // Metric
        tfHeightMetric.text = String(Int(heightCm))
        tfChestMetric.text = String(Int(chestCm))
        tfWaistMetric.text = String(Int(waistCm))
        tfHipsMetric.text = String(Int(hipsCm))
        
        // Imperial
        tfHeightFeet.text = String(Int(heightFeet))
        tfHeightInches.text = String(Int(heightInches))
        tfChestImperial.text = String(Int(chestInches))
        tfWaistImperial.text = String(Int(waistInches))
        tfHipsImperial.text = String(Int(hipsInches))
    }
    
    private func initImperialInputs() {
        lbHeightFeet.text = localizedStringBundle(key: "labels.feet", for: self.classForCoder)
        lbErrorFeet.text = ""
        lbErrorInches.text = ""
        lbChestImperial.text = localizedStringBundle(key: "measurements.chest", for: self.classForCoder)
        lbErrorChestImperial.text = ""
        lbWaistImperial.text = localizedStringBundle(key: "measurements.waist", for: self.classForCoder)
        lbErrorWaistImperial.text = ""
        lbHipsImperial.text = localizedStringBundle(key: "measurements.hips", for: self.classForCoder)
        lbErrorHipsImperial.text = ""
        tfHeightFeet.keyboardType = .numberPad
        tfHeightInches.keyboardType = .numberPad
        tfWaistImperial.keyboardType = .numberPad
        tfChestImperial.keyboardType = .numberPad
        tfHipsImperial.keyboardType = .numberPad
    }
    private func initSliderControl() {
        lbFitControl.text = localizedStringBundle(key: "title.fitting", for: self.classForCoder)
        btnTighter.setTitle("< " + localizedStringBundle(key: "fitting.1", for: self.classForCoder).capitalized, for: .normal)
        btnLooser.setTitle(localizedStringBundle(key: "fitting.3", for: self.classForCoder).capitalized + " >", for: .normal)
        sliderFitControl.value = Float(fitValue) //SharedBetweenFormsData.shared.fit ?? Float(subjectDataMetric?.fit ?? 2)
    }
    
    // MARK: - IBActions
    @IBAction func onSystemMetricChange(_ sender: Any) {
        isImperial = swMetricSystem.isOn
        SharedBetweenFormsData.shared.isImperial = isImperial
        switchForm()
    }
    @IBAction func onFitSliderChange(_ sender: UISlider) {
        let step: Float = SliderFitValues.STEP.rawValue
        let currentValue = round((sender.value - sender.minimumValue) / step)
        sender.value = currentValue
        btnTighter.isEnabled = currentValue > SliderFitValues.MIN.rawValue
        btnLooser.isEnabled = currentValue < SliderFitValues.MAX.rawValue
        fitValue = Int(currentValue)
        sexProfile?.fit = fitValue
    }
    // BUTTONS
    @IBAction func onBtnTightPress(_ sender: UIButton) {
        if (sliderFitControl.value < SliderFitValues.MIN.rawValue) {
            sliderFitControl.value = SliderFitValues.MIN.rawValue
        } else {
            sliderFitControl.value -= 1.0
        }
    }
    @IBAction func onBtnLooserPress(_ sender: UIButton) {
        if (sliderFitControl.value > SliderFitValues.MAX.rawValue) {
            sliderFitControl.value = SliderFitValues.MAX.rawValue
        } else {
            sliderFitControl.value += 1.0
        }
    }
    @IBAction func onBtnNavToBasicPress(_ sender: UIButton) {
        persistSharedData()
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let mainForm = storyboard.instantiateViewController(withIdentifier: "MainFormViewController") as! MainFormViewController
        mainForm.contentView = contentView
        mainForm.popupData = popupData
        mainForm.product = self.product
        contentView!.show(from: self, to: mainForm)
    }
    @IBAction func onBtnCountinuePress(_ sender: UIButton) {
        var isValidForm: Bool = true
        
        if (isImperial) {
            // check y set de errores antes de validar, por datos precargados
            scanAllImperialInputs()
            // validate imperial fields
            isValidForm = !FormValidationHelper.someHasError(labels: lbErrorFeet, lbErrorInches, lbErrorChestMetric, lbErrorWaistMetric, lbErrorHipsMetric)
            
        } else {
            // check y set de errores antes de validar, por datos precargados
            scanAllMetricInputs()
            // validate metric fields
            isValidForm = !FormValidationHelper.someHasError(labels: lbErrorHeightMetric, lbErrorChestMetric, lbErrorWaistMetric, lbErrorHipsMetric)
        }
        
        if isValidForm {
            SharedBetweenFormsData.shared.isFromAdvanced = true
            persistSharedData()
            navigateToLastStep()
        }
    }
    
    private func persistSharedData() {
        let _chest = SharedBetweenFormsData.Measure(metric: chestCm, imperial: chestInches)
        let _waist = SharedBetweenFormsData.Measure(metric: waistCm, imperial: waistInches)
        let _hips = SharedBetweenFormsData.Measure(metric: hipsCm, imperial: hipsInches)
  
        sexProfile?.chest = _chest
        sexProfile?.waist = _waist
        sexProfile?.hips = _hips
        sexProfile?.metricHeight = heightCm
        sexProfile?.imperialHeight = HeightImperial(ft: heightFeet, inX: heightInches)
        sexProfile?.fit = fitValue
    }
    
    // MARK: - IBActions TextFields Metric
    @IBAction func onHeightMetricDidEdit(_ sender: UITextField) {
        updateHeightMetricInput(sender)
    }
    @IBAction func onHeightMetricChange(_ sender: UITextField) {
       updateHeightMetricInput(sender)
    }
    
    private func updateHeightMetricInput(_ sender: UITextField) {
        var heightAux: Any = heightCm
        FormValidationHelper.setInputError(text: sender.text!,measureLimit: (limitsMetric?.height!)!, errorLabel: lbErrorHeightMetric, value: &heightAux)
        heightCm = heightAux as! Float
        sexProfile?.metricHeight = heightCm
    }
    
    @IBAction func onChestMetricDidEdit(_ sender: UITextField) {
        updateChestInput(sender)
    }
    @IBAction func onChestMetricChange(_ sender: UITextField) {
        updateChestInput(sender)
    }
    
    private func updateChestInput(_ sender: UITextField) {
        if (SharedBetweenFormsData.shared.isImperial) {
            var chestInAux: Any = chestInches
            FormValidationHelper.setInputError(text: sender.text!, measureLimit: (limitsImperial?.chest!)!, errorLabel: lbErrorChestImperial, value: &chestInAux)
            chestInches = chestInAux as! Float
            sexProfile?.chest.imperial = chestInches
        } else {
            var chestAux: Any = chestCm
            FormValidationHelper.setInputError(text: sender.text!,measureLimit: (limitsMetric?.chest!)!, errorLabel: lbErrorChestMetric, value: &chestAux)
            chestCm = chestAux as! Float
            sexProfile?.chest.metric = chestCm
        }
    }
    
    @IBAction func onWaistMetricDidEdit(_ sender: UITextField) {
        updateWaistInput(sender)
    }
    @IBAction func onWaistMetricChange(_ sender: UITextField) {
        updateWaistInput(sender)
    }
    
    private func updateWaistInput(_ sender: UITextField) {
        if (SharedBetweenFormsData.shared.isImperial) {
            var waistInAux: Any = waistInches
            FormValidationHelper.setInputError(text: sender.text!, measureLimit: (limitsImperial?.waist!)!, errorLabel: lbErrorWaistImperial, value: &waistInAux)
            waistInches = waistInAux as! Float
            sexProfile?.waist.imperial = waistInches
        } else {
            var waistAux: Any = waistCm
            FormValidationHelper.setInputError(text: sender.text!, measureLimit: (limitsMetric?.waist!)!, errorLabel: lbErrorWaistMetric, value: &waistAux)
            waistCm = waistAux as! Float
            sexProfile?.waist.metric = waistCm
        }
    }
    
    @IBAction func onHipsMetricDidEdit(_ sender: UITextField) {
        updateHipsInput(sender)
    }
    @IBAction func onHipsMetricChange(_ sender: UITextField) {
        updateHipsInput(sender)
    }
    
    private func updateHipsInput(_ sender: UITextField) {
        if (SharedBetweenFormsData.shared.isImperial) {
            var hipsInAux: Any = hipsInches
            FormValidationHelper.setInputError(text: sender.text!, measureLimit: (limitsImperial?.hips!)!, errorLabel: lbErrorWaistImperial, value: &hipsInAux)
            hipsInches = hipsInAux as! Float
            sexProfile?.hips.imperial = hipsInches
        } else {
            var hipsAux: Any = hipsCm
            FormValidationHelper.setInputError(text: sender.text!, measureLimit: (limitsMetric?.hips!)!, errorLabel: lbErrorHipsMetric, value: &hipsAux)
            hipsCm = hipsAux as! Float
            sexProfile?.hips.metric = hipsCm
        }
    }
   
    // MARK: - IBActions TextFields Imperial
    @IBAction func onFeetImperialDidEdit(_ sender: UITextField) {
        updateFeetInput(sender)
    }
    @IBAction func onFeetImperialChange(_ sender: UITextField) {
        updateFeetInput(sender)
    }
    
    private func updateFeetInput(_ sender: UITextField) {
        _ = manageHeightFeetErrorMessage(text: sender.text)
        
        if !isNilOrBlank(str: sender.text) {
            let inputValue = (sender.text! as NSString).floatValue
            intPartFeet = Int(inputValue)
            heightFeet = Float(intPartFeet) + decPartFeet
            sexProfile?.imperialHeight?.ft = heightFeet
        }
    }
    
    @IBAction func onInchesImperialDidEdit(_ sender: UITextField) {
        updateInchesImperial(sender)
    }
    @IBAction func onInchesImperialChange(_ sender: UITextField) {
        updateInchesImperial(sender)
    }
    
    private func updateInchesImperial(_ sender: UITextField) {
        _ = manageHeightInchesErrorMessage(text: sender.text)
        
        if !isNilOrBlank(str: sender.text) {
            let inputValue = (sender.text! as NSString).floatValue
            intPartIn = Int(inputValue)
            heightInches = Float(intPartIn) + decPartIn
            sexProfile?.imperialHeight?.inX = heightInches
        }
    }
    
    @IBAction func onChestImperialDidEdit(_ sender: UITextField) {
        updateChestInput(sender)
    }
    @IBAction func onChestImperialChange(_ sender: UITextField) {
        updateChestInput(sender)
    }
    @IBAction func onWaistImperialDidEdit(_ sender: UITextField) {
        updateWaistInput(sender)
    }
    @IBAction func onWaistImperialChange(_ sender: UITextField) {
        updateWaistInput(sender)
    }
    
    @IBAction func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            textField.selectAll(nil)
        }
    }
    
    @IBAction func onHipsImperialDidEdit(_ sender: UITextField) {
        updateHipsInput(sender)
    }
    @IBAction func onHipsImperialChange(_ sender: UITextField) {
        updateHipsInput(sender)
    }
    
    // MARK: - Render Helpers
    private func switchForm() {
        showMetricInputs(show: !isImperial)
        showImperialInputs(show: isImperial)
    }
    private func showMetricInputs(show: Bool) {
        viewMetricInputs.isHidden = !show
    }
    private func showImperialInputs(show: Bool) {
        viewImperialInputs.isHidden = !show
    }
    private func renderIfMeasurement() {
        hideAllInputs()
        if !measuresList!.isEmpty {
            if measuresList!.contains(MeasurementsValues.HEIGHT.rawValue) {
                hideHeightMetric(hide: false)
                hideHeightImperial(hide: false)
            }
            if measuresList!.contains(MeasurementsValues.WAIST.rawValue) {
                hideWaistMetric(hide: false)
                hideWaistImperial(hide: false)
            }
            if measuresList!.contains(MeasurementsValues.CHEST.rawValue) {
                hideChestMetric(hide: false)
                hideChestImperial(hide: false)
            }
            if measuresList!.contains(MeasurementsValues.HIPS.rawValue) {
                hideHipsMetric(hide: false)
                hideHipsImperial(hide: false)
            }
        }
    }
    
    // MARK: - Inputs group visibility methods
    private func hideAllInputs() {
        hideHeightMetric(hide: true)
        hideChestMetric(hide: true)
        hideWaistMetric(hide: true)
        hideHipsMetric(hide: true)
        hideHeightImperial(hide: true)
        hideChestImperial(hide: true)
        hideWaistImperial(hide: true)
        hideHipsImperial(hide: true)
    }
    // METRIC
    private func hideHeightMetric(hide: Bool) {
        if (hide && !lbHeightMetric.isHidden){
            metricViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            heightMetricViewHeightConstraint.constant = 0
        }else if  (!hide && lbHeightMetric.isHidden){
            metricViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            heightMetricViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbHeightMetric.isHidden = hide
        lbErrorHeightMetric.isHidden = hide
        tfHeightMetric.isHidden = hide
        heightCmTag.isHidden = hide
    }
    private func hideChestMetric(hide: Bool) {
        if (hide && !lbChestMetric.isHidden){
            metricViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            chestMetricViewHeightConstraint.constant = 0
        }else if  (!hide && lbChestMetric.isHidden){
            metricViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            chestMetricViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbChestMetric.isHidden = hide
        lbErrorChestMetric.isHidden = hide
        tfChestMetric.isHidden = hide
        chestCmTag.isHidden = hide
    }
    private func hideWaistMetric(hide: Bool) {
        if (hide && !lbWaistMetric.isHidden){
            metricViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            waistMetricViewHeightConstraint.constant = 0
        }else if  (!hide && lbWaistMetric.isHidden){
            metricViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            waistMetricViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbWaistMetric.isHidden = hide
        lbErrorWaistMetric.isHidden = hide
        tfWaistMetric.isHidden = hide
        waistCmTag.isHidden = hide
    }
    private func hideHipsMetric(hide: Bool) {
        if (hide && !lbHipsMetric.isHidden){
            metricViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            hipsMetricViewHeightConstraint.constant = 0
        }else if  (!hide && lbHipsMetric.isHidden){
            metricViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            hipsMetricViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbHipsMetric.isHidden = hide
        lbErrorHipsMetric.isHidden = hide
        tfHipsMetric.isHidden = hide
        hipsCmTag.isHidden = hide
    }
    
    private func hideGenreSegmentedBtn(hide: Bool) {
        lbGenreAdvance.isHidden = hide
        sbGenreAdv.isHidden = hide
        ctLbGenreAdv.constant = 0
        ctSbGenreAdv.constant = 0
    }
    
    // IMPERIAL
    private func hideHeightImperial(hide: Bool) {
        if (hide && !lbHeightFeet.isHidden){
            imperialViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            heightImperialViewHeightConstraint.constant = 0
        }else if  (!hide && lbHeightFeet.isHidden){
            imperialViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            heightImperialViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbHeightFeet.isHidden = hide
        lbErrorFeet.isHidden = hide
        lbErrorInches.isHidden = hide
        tfHeightFeet.isHidden = hide
        tfHeightInches.isHidden = hide
        heightFtTag.isHidden = hide
        heightInTag.isHidden = hide
    }
    private func hideChestImperial(hide: Bool) {
        if (hide && !lbChestImperial.isHidden){
            imperialViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            chestImperialViewHeightConstraint.constant = 0
        }else if  (!hide && lbChestImperial.isHidden){
            imperialViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            chestImperialViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbChestImperial.isHidden = hide
        lbErrorChestImperial.isHidden = hide
        tfChestImperial.isHidden = hide
        chestInTag.isHidden = hide
    }
    private func hideWaistImperial(hide: Bool) {
        if (hide && !lbWaistImperial.isHidden){
            imperialViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            waistImperialViewHeightConstraint.constant = 0
        }else if  (!hide && lbWaistImperial.isHidden){
            imperialViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            waistImperialViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbWaistImperial.isHidden = hide
        lbErrorWaistImperial.isHidden = hide
        tfWaistImperial.isHidden = hide
        waistInTag.isHidden = hide
    }
    private func hideHipsImperial(hide: Bool) {
        if (hide && !lbHipsImperial.isHidden){
            imperialViewHeightConstraint.constant -= INPUT_HEIGHT
            contentViewHeightContraint.constant -= INPUT_HEIGHT
            hipsImperialViewHeightConstraint.constant = 0
        }else if  (!hide && lbHipsImperial.isHidden){
            imperialViewHeightConstraint.constant += INPUT_HEIGHT
            contentViewHeightContraint.constant += INPUT_HEIGHT
            hipsImperialViewHeightConstraint.constant = INPUT_HEIGHT
        }
        
        lbHipsImperial.isHidden = hide
        lbErrorHipsImperial.isHidden = hide
        tfHipsImperial.isHidden = hide
        hipsInTag.isHidden = hide
    }
    
    // MARK: - Self Utils Helpers
    private func manageHeightInchesErrorMessage(text: String?) -> Bool {
        let inches = Int(text ?? "0") ?? 03
        setCurrentInches(feet: tfHeightFeet.text ?? "0", inches: String(inches))
        var hasError = false
        let isTextEmptyOrNil = text!.isEmpty || text == nil
        
        if (isTextEmptyOrNil || currentInchesHeight < Int(minInchesHeight) || currentInchesHeight > Int(maxInchesHeight) || inches > 11) {
            let minFt = Int((limitsImperial?.height?.min?.ft)!)
            let maxFt = Int((limitsImperial?.height?.max?.ft)!)
            let minInx = Int((limitsImperial?.height?.min?.inX)!)
            let maxInx = Int((limitsImperial?.height?.max?.inX)!)
            lbErrorFeet.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            lbErrorInches.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            hasError = true
        } else {
            lbErrorFeet.text = ""
            lbErrorInches.text = ""
        }
        return hasError
    }
    
    private func manageHeightFeetErrorMessage(text: String?) -> Bool {
        setCurrentInches(feet: text ?? "0", inches: tfHeightInches.text ?? "0")
        var hasError = false
        let isTextEmptyOrNil = text!.isEmpty || text == nil
        if (isTextEmptyOrNil || currentInchesHeight < Int(minInchesHeight) || currentInchesHeight > Int(maxInchesHeight)) {
            let minFt = Int((limitsImperial?.height?.min?.ft)!)
            let maxFt = Int((limitsImperial?.height?.max?.ft)!)
            let minInx = Int((limitsImperial?.height?.min?.inX)!)
            let maxInx = Int((limitsImperial?.height?.max?.inX)!)
            lbErrorFeet.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            lbErrorInches.text = "\(minFt) ft. \(minInx) in. - \(maxFt) ft. \(maxInx) in."
            hasError = true
        } else {
            lbErrorFeet.text = ""
            lbErrorInches.text = ""
        }
        return hasError
    }
    
    private func setCurrentInches(feet: String, inches: String) {
        let heightFeet = Int(feet) ?? 0
        let feetInches: Int = heightFeet * 12
        let inches: Int = Int(inches) ?? 0
        currentInchesHeight = feetInches + inches
    }
    
    private func getSaveRecommendRequest() -> SaveRecommendRequest{
        let request = SaveRecommendRequest(product: product)

        request.ck = popupData?.ck
        request.sex = selectedSex ?? popupData?.sex
        request.fit = fitValue
        request.w = RecommendationWay.ADV_WAY.rawValue
        
        if (!isImperial){
            request.height = heightCm > 0 ? String(heightCm) : String((tfHeightMetric.text! as NSString).floatValue)
            request.chest = chestCm > 0 ? String(chestCm) : String((tfChestMetric.text! as NSString).floatValue)
            request.waist = waistCm > 0 ? String(waistCm) : String((tfWaistMetric.text! as NSString).floatValue)
            request.hips = hipsCm > 0 ? String(hipsCm) : String((tfHipsMetric.text! as NSString).floatValue)
            request.metric_system = MetricSystem.METRIC.rawValue
            request.mass_system = MetricSystem.METRIC.rawValue
        } else {
            request.height = heightTotalInches > 0 ? String(heightTotalInches) : String((tfHeightFeet.text! as NSString).floatValue * 12 + (tfHeightInches.text! as NSString).floatValue)
            request.chest = chestInches > 0 ? String(chestInches) : String((tfChestImperial.text! as NSString).floatValue)
            request.waist = waistInches > 0 ? String(waistInches) : String((tfWaistImperial.text! as NSString).floatValue)
            request.hips = hipsInches > 0 ? String(hipsInches) : String((tfHipsImperial.text! as NSString).floatValue)
            request.metric_system = MetricSystem.IMPERIAL.rawValue
            request.mass_system = MetricSystem.IMPERIAL.rawValue
        }
        return request
    }
    
    private func  navigateToLastStep() {
        let request = getSaveRecommendRequest()
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let lastStep = storyboard.instantiateViewController(withIdentifier: "LastStepViewController") as! LastStepViewController
        
        lastStep.contentView = contentView
        lastStep.popupData = popupData
        lastStep.product = self.product
        lastStep.request = request
        
        contentView!.show(from: self, to: lastStep)
    }
    
    private func scanAllMetricInputs() {
        // Height
        if limitsMetric?.height != nil {
            var heightAux: Any = heightCm
            FormValidationHelper.setInputError(text: tfHeightMetric.text!,measureLimit: (limitsMetric?.height!)!, errorLabel: lbErrorHeightMetric, value: &heightAux)
            heightCm = heightAux as! Float
        }
        // Chest
        if limitsMetric?.chest != nil {
            var chestAux: Any = chestCm
            FormValidationHelper.setInputError(text: tfChestMetric.text!,measureLimit: (limitsMetric?.chest!)!, errorLabel: lbErrorChestMetric, value: &chestAux)
            chestCm = chestAux as! Float
        }
        // Waist
        if limitsMetric?.waist != nil {
            var waistAux: Any = waistCm
            FormValidationHelper.setInputError(text: tfWaistMetric.text!, measureLimit: (limitsMetric?.waist!)!, errorLabel: lbErrorWaistMetric, value: &waistAux)
            waistCm = waistAux as! Float
        }
        // Hips
        if limitsMetric?.hips != nil {
            var hipsAux: Any = hipsCm
            FormValidationHelper.setInputError(text: tfHipsMetric.text!, measureLimit: (limitsMetric?.hips!)!, errorLabel: lbErrorHipsMetric, value: &hipsAux)
            hipsCm = hipsAux as! Float
        }
    }
    
    private func scanAllImperialInputs() {
        // feet
        _ = manageHeightFeetErrorMessage(text: tfHeightFeet.text)
        // inches
        _ = manageHeightInchesErrorMessage(text: tfHeightInches.text)
        // chest
        if limitsImperial?.chest != nil {
            var chestInAux: Any = chestInches
            FormValidationHelper.setInputError(text: tfChestImperial.text!, measureLimit: (limitsImperial?.chest!)!, errorLabel: lbErrorChestImperial, value: &chestInAux)
            chestInches = chestInAux as! Float
        }
        // waist
        if limitsImperial?.waist != nil {
            var waistInAux: Any = waistInches
            FormValidationHelper.setInputError(text: tfWaistImperial.text!, measureLimit: (limitsImperial?.waist!)!, errorLabel: lbErrorWaistImperial, value: &waistInAux)
            waistInches = waistInAux as! Float
        }
        // hips
        if limitsImperial?.hips != nil {
            var hipsInAux: Any = hipsInches
            FormValidationHelper.setInputError(text: tfHipsImperial.text!, measureLimit: (limitsImperial?.hips!)!, errorLabel: lbErrorWaistImperial, value: &hipsInAux)
            hipsInches = hipsInAux as! Float
        }
    }
    
    private func updateDataAndViewsOnSexChange() {
        updateSubjectData()
        initMeasuresValues()
        setDefaultFormValues()
        initSliderControl()
    }
}
