//
//  Network.swift
//  Pods
//
//  Created by Pedro Aquilino on 30/8/24.
//

import Foundation

struct Network {
    static let session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        
        return URLSession(configuration: configuration)
    }()
    
    static func send<T: Decodable>(request: URLRequestProtocol, completion: @escaping (Result<T, Error>) -> Void) {
        // debugPrint(request)
        Network.session.dataTask(with: request.urlRequest) { (data, _, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data else {
                    completion(.failure(NetworkError.emptyResponse))
                    return
                }
                if (data.isEmpty) {
                    completion(.failure(NetworkError.emptyResponse))
                    return
                }
                
                // let str = String(data: data, encoding: .utf8)
                // debugPrint(str!)
                    
                do {
                    let responseObject = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(responseObject))
                } catch {
                    completion(.failure(error))
                }
            }
        }.resume()
    }
    
    static func send_api(request: URLRequestProtocol, completion: @escaping (Result<String, Error>) -> Void) {
        Network.session.dataTask(with: request.urlRequest) { (data, _, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let data = data else {
                    completion(.success(""))
                    return
                }
                if (data.isEmpty) {
                    completion(.success(""))
                    return
                }
                // let str = String(data: data, encoding: .utf8)
                // debugPrint(str!)
                do {
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(ConfirmResponseErrors.self, from: data)
                    completion(.failure(result))
                } catch {
                    completion(.failure(error))
                }
                return
            }
        }.resume()
    }
}

enum NetworkError: Error {
    case emptyResponse
}
