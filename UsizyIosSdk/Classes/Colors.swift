//
//  Colors.swift
//  UsizyIosSdk
//
//  Created by Daniel Almería Núñez on 10/03/2021.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }

    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
    
    static var defaultColor = UIColor(rgb: 0x63AF91, a: 1)
    static var colorPrimary = UIColor(rgb: 0x000000, a: 1)
    static var colorPrimaryDark = UIColor(rgb: 0x000000, a: 1)
    static var colorSecondary = UIColor(rgb: 0x4D4D4D, a: 1)
    static var colorAccent = UIColor(rgb: 0x000000, a: 1)
    static var colorButtonText = UIColor(rgb: 0xFFFFFF, a: 1)
    static var toggleOutline = UIColor(rgb: 0xCFD7E0, a: 1)
    static var toggleCircle = UIColor(rgb: 0x3C3C3B, a: 1)
}



