//
//  ShoesFormViewController.swift
//  UsizyIosSdk
//
//  Created by Daniel Almería Núñez on 13/4/21.
//

import UIKit

class ShoesFormViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    private let reuseIdentifier = "ShoeFavCell"
    private let itemsPerRow: CGFloat = 2
    
    private let sectionInsets = UIEdgeInsets(
      top: 0.0,
      left: 20.0,
      bottom: 20.0,
      right: 20.0)
    
    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var product: String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var ivProductMain: UIImageView!
    @IBOutlet weak var lbHeader: UILabel!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btAdvanced: UIButton!
    @IBOutlet weak var cvShoeFavs: UICollectionView!
    
    @IBOutlet weak var ivHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ivWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var vhHeaderHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Private vars
    private var imageProductURL: String?
    private var shoefavs:[ShoeFavs] = []
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        showDefaultShoefavs()
    }
    
    // MARK: - Views and variables Initializers
    private func initVariables() {
        imageProductURL = popupData?.image ?? nil
    }
    
    private func initViews() {
        cvShoeFavs.delegate = self
        cvShoeFavs.dataSource = self
        
        tfSearch.delegate = self
        tfSearch.attributedPlaceholder = NSAttributedString(string: localizedStringBundle(key: "labels.shoesearch", for: self.classForCoder),
             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        lbHeader.text = localizedStringBundle(key: "title.shoebrand", for: self.classForCoder)
        btAdvanced.titleLabel?.lineBreakMode = .byWordWrapping
        btAdvanced.titleLabel?.numberOfLines = 2
        btAdvanced.underlineButton(text: localizedStringBundle(key: "buttons.advshoes", for: self.classForCoder))
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.clickOutside(sender:)))
        view.addGestureRecognizer(gesture)
        gesture.cancelsTouchesInView = false
        
        initImageView()
    }
    
    private func initImageView() {
        if !isNilOrBlank(str: imageProductURL) {
            requestImage(url: imageProductURL!) { image in
                self.ivProductMain.image = image
            }
        } else {
            ivProductMain.isHidden = true
            ivHeightConstraint.constant = 0
            ivWidthConstraint.constant = 0
            vhHeaderHeightConstraint.constant = 70
            ivProductMain.clipsToBounds = true
        }
    }
    
    private func showDefaultShoefavs(){
        if (popupData != nil && popupData?.shoe_favs != nil){
            shoefavs = popupData!.shoe_favs!
            self.cvShoeFavs.reloadData()
        }
    }
    
    @IBAction func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            textField.selectAll(nil)
        }
    }
    
    @objc func clickOutside(sender : UITapGestureRecognizer) {
        tfSearch.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @IBAction func endEditing(_ sender: Any) {
        if (isNilOrBlank(str: tfSearch.text)){
            showDefaultShoefavs()
        }else{
            fetchData()
        }
    }
    
    @IBAction func onChangeValue(_ sender: Any) {
        endEditing(sender)
    }
    
    // MARK: - IBAction
    @IBAction func showAdvanced(_ sender: Any) {
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let mainForm = storyboard.instantiateViewController(withIdentifier: "ShoesMeasuresViewController") as! ShoesMeasuresViewController
        mainForm.contentView = contentView
        mainForm.popupData = popupData
        mainForm.product = self.product
        contentView!.show(from: self, to: mainForm)
    }
    
    // MARK: - Internal
    private func fetchData(){
        let request = ShoeBrandRequest(product: self.product)
        request.q = tfSearch.text
        request.sex = (popupData?.sex == SexValues.FEMALE.rawValue) ? popupData?.sex : SexValues.MALE.rawValue
        SizeQuery().shoeBrand(request) { (response: ShowBrandResponse?) in
            let storyboard = loadStoryboardBundle(for: self.classForCoder)
            
            if (response != nil && response!.results != nil){
                self.shoefavs = response!.results!
                self.cvShoeFavs.reloadData()
            }else{
                let errorForm = storyboard.instantiateViewController(withIdentifier: "ErrorViewController") as! ErrorViewController
                errorForm.contentView = self.contentView
                errorForm.popupData = self.popupData
                errorForm.product = self.product
                self.contentView?.show(from: self, to: errorForm)
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource
     func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1
     }
     
     func collectionView(
       _ collectionView: UICollectionView,
       numberOfItemsInSection section: Int
     ) -> Int {
       return shoefavs.count
     }
     
    func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier,
            for: indexPath
          ) as! ShoeFavCollectionViewCell

        let item = self.shoefavs[indexPath.row]
        cell.lbText.text = item.text
        cell.ivThumb.image = nil
        
        if !isNilOrBlank(str: item.thumb) {
            requestImage(url: item.thumb!) { image in
                cell.ivThumb.image = image
            }
        }
    
      return cell
    }
    
     func collectionView(
       _ collectionView: UICollectionView,
       layout collectionViewLayout: UICollectionViewLayout,
       sizeForItemAt indexPath: IndexPath
     ) -> CGSize {
       let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
       let availableWidth = view.frame.width - paddingSpace
       let widthPerItem = availableWidth / itemsPerRow

       return CGSize(width: widthPerItem, height: widthPerItem)
     }
    
     func collectionView(
       _ collectionView: UICollectionView,
       layout collectionViewLayout: UICollectionViewLayout,
       insetForSectionAt section: Int
     ) -> UIEdgeInsets {
       return sectionInsets
     }
     
     func collectionView(
       _ collectionView: UICollectionView,
       layout collectionViewLayout: UICollectionViewLayout,
       minimumLineSpacingForSectionAt section: Int
     ) -> CGFloat {
       return sectionInsets.left
     }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.shoefavs[indexPath.row]
        
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let shoeSizeSelect = storyboard.instantiateViewController(withIdentifier: "ShoesSizeSelectViewController") as! ShoesSizeSelectViewController
        shoeSizeSelect.contentView = contentView
        shoeSizeSelect.popupData = popupData
        shoeSizeSelect.product = self.product
        shoeSizeSelect.shoeFav = item
        contentView!.show(from: self, to: shoeSizeSelect)
    }
    
}
