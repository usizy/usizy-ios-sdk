//
//  Utils.swift
//  UsizyIosSdk
//
//  Created by Daniel Almería Núñez on 10/03/2021.
//

import Foundation
import UIKit

public func loadImageBundle(named imageName:String, for aClass: AnyClass) -> UIImage? {
    let podBundle = Bundle(for: aClass)
    if let bundleURL = podBundle.url(forResource: "UsizyIosSdk", withExtension: "bundle")
    {
        let imageBundel = Bundle(url:bundleURL )
        let image = UIImage(named: imageName, in: imageBundel, compatibleWith: nil)
        return image
    }
    return nil
}

public func localizedStringBundle(key: String, for aClass: AnyClass) -> String{
    let path = Bundle(for: aClass).path(forResource: "UsizyIosSdk", ofType: "bundle")
    let bundle = path != nil ? Bundle(path: path!)! : Bundle(for: aClass)
    return NSLocalizedString(key, tableName: "Localizable", bundle:bundle, value: "", comment: "")
}

public func loadStoryboardBundle(for aClass: AnyClass) -> UIStoryboard{
    let path = Bundle(for: aClass).path(forResource: "UsizyIosSdk", ofType: "bundle")
    let bundle = path != nil ? Bundle(path: path!)! : Bundle(for: aClass)
    let storyboard = UIStoryboard(name: "UsizyMain", bundle: bundle)
    return storyboard
}

public func isNilOrBlank(str: String?) -> Bool {
    if str == nil {
        return true
    }
    if str!.isEmpty {
        return true
    }
    return false
}

public func requestImage(url: String, completion: @escaping (UIImage) -> Void) {
    DispatchQueue.global(qos: .background).async {
        do {
            let data = try Data.init(contentsOf: URL.init(string:url)!)
                DispatchQueue.main.async {
                    let image: UIImage = UIImage(data: data)!
                    completion(image)
                }
       } catch {
           print("Error requesting image \(error)")
       }
    }
}

extension UIButton {
  func underlineText() {
    guard let title = title(for: .normal) else { return }

    let titleString = NSMutableAttributedString(string: title)
    titleString.addAttribute(
      .underlineStyle,
      value: NSUnderlineStyle.single.rawValue,
      range: NSRange(location: 0, length: title.count)
    )
    setAttributedTitle(titleString, for: .normal)
  }
}

extension UIButton {
    func underlineButton(text: String) {
        let titleString = NSMutableAttributedString(string: text)
        titleString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, text.count))
        self.setAttributedTitle(titleString, for: .normal)
    }
}
