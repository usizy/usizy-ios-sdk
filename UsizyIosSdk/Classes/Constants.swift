//
//  Constants.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 02/03/2021.
//

import Foundation


enum ShowProduct: String, Decodable{
    case APP = "show_product_app"
    case EMPTY = "show_empty"
    case ERROR = "show_product_error"
}

enum SizeResult: String {
    case MATCH = "MATCH"
    case OVERFLOW = "OVERFLOW"
    case UNDERFLOW = "UNDERFLOW"
}

enum MeasurementsValues: String {
    case HEIGHT = "height"
    case CHEST = "chest"
    case WAIST = "waist"
    case HIPS = "hips"
}

enum SexValues: String {
    case BABY = "baby"
    case BOY = "boy"
    case CHILD = "child"
    case GIRL = "girl"
    case FEMALE = "female"
    case MALE = "male"
    case HUMAN = "human"
    case UNISEX = "unisex"
}

enum ZoneValues: String {
    case UPPER = "upper"
    case LOWER = "lower"
    case FULL = "full"
}

enum MetricSystem: String {
    case IMPERIAL = "imperial"
    case METRIC = "metric"
}

enum RecommendationWay: String {
    case BASIC_WAY = "basic"
    case ADV_WAY = "adv"
}

enum SystemConversionFactors: Float {
    case CONVERSION_FACTOR_A = 2.54
    case CONVERSION_FACTOR_B = 12.0
    case CONVERSION_WEIGHT_FACTOR = 0.45359237
}

enum SliderFitValues: Float {
    case MIN = 0.0
    case MAX = 4.0
    case STEP = 1.0
}

enum PopupZones: String {
    case SHOES = "shoes"
    case LOWER = "lower"
    case UPPER = "upper"
    case FULL = "full"
}
