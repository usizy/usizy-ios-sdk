//
//  PrivacyViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 01/03/2021.
//

import UIKit
import SafariServices

class PrivacyViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var btPrivacy: UIButton!
    @IBOutlet weak var btTerms: UIButton!
    @IBOutlet weak var btVisit: UIButton!
    @IBOutlet weak var vwContainer: UIView!
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeAppearance()
        localized()
    }
    
    // MARK: - IBAction
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showPrivacy(_ sender: Any) {
        showUrl("privacy.href1")
    }
    
    @IBAction func showTerms(_ sender: Any) {
        showUrl("privacy.href2")
    }
    
    @IBAction func showLanding(_ sender: Any) {
        showUrl("privacy.link")
    }
    
    // MARK: - Internal
    private func initializeAppearance(){
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        view.isOpaque = false
        vwContainer.layer.cornerRadius = 10
    }
    
    private func localized(){
        lbTitle.text = localizedStringBundle(key: "privacy.title", for: self.classForCoder)
        lbSubtitle.text = localizedStringBundle(key: "privacy.subtitle", for: self.classForCoder)
        btPrivacy.setTitle(localizedStringBundle(key: "privacy.textHref1", for: self.classForCoder), for: .normal)
        btTerms.setTitle(localizedStringBundle(key: "privacy.textHref2", for: self.classForCoder), for: .normal)
        btVisit.setTitle(localizedStringBundle(key: "buttons.goto", for: self.classForCoder).uppercased(), for: .normal)
    }
    
    private func showUrl(_ key: String){
        let urlString = localizedStringBundle(key: key, for: self.classForCoder)
        if let url = URL(string: urlString) {
            let svc = SFSafariViewController(url: url)
            present(svc, animated: true, completion: nil)
        }
    }
}
