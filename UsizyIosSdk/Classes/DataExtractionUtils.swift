//
//  File.swift
//  Pods
//
//  Created by David Arcis on 5/3/21.
//

import Foundation

class DataExtractionUtils {
    
    /**
        Retrieves the metric `SubjectDataMetric` depending on the `sex` and  `selectedSex` values.
            This is an overloaded method for just metric cases.

        - Parameter dataMetric:   The data set where to extract from.
        - Parameter sex: The SexValue in String.
        - Parameter selectedSex: Optional parameter, used when sex is Human or Unisex.

        - Returns: The proper `SubjectDataMetric` data if the sex filters matches, `nil`otherwise.
    */
    static func getSubjectDataBySex(dataMetric: DataMetric, sex: String, selectedSex: String?) -> SubjectDataMetric? {
        var subjectData: SubjectDataMetric = SubjectDataMetric()
        
        switch sex {
        case SexValues.BABY.rawValue:
            subjectData = dataMetric.baby!
        case SexValues.CHILD.rawValue:
            subjectData =  dataMetric.child!
        case SexValues.BOY.rawValue:
            subjectData =  dataMetric.boy!
        case SexValues.GIRL.rawValue:
            subjectData =  dataMetric.girl!
        case SexValues.FEMALE.rawValue:
            subjectData = dataMetric.female!
        case SexValues.MALE.rawValue:
            subjectData = dataMetric.male!
        case SexValues.HUMAN.rawValue:
            if selectedSex != nil {
                subjectData = getSexForHumanType(selectedSex: selectedSex!, dataMetric: dataMetric)
            }
        case SexValues.UNISEX.rawValue:
            if selectedSex != nil {
            return getSexForUnisexType(selectedSex: selectedSex!, dataMetric: dataMetric)
            }
        default:
            return nil
        }
        return subjectData
    }
    
    static func getSexForHumanType(selectedSex: String, dataMetric: DataMetric) -> SubjectDataMetric {
        if selectedSex == SexValues.BOY.rawValue {
            return dataMetric.boy!
        }
        return dataMetric.male!
    }
    
    static func getSexForUnisexType(selectedSex: String, dataMetric: DataMetric) -> SubjectDataMetric {
        if selectedSex == SexValues.FEMALE.rawValue {
            return dataMetric.female!
        }
        return dataMetric.male!
    }
    
    /**
        Retrieves the metric `SubjectDataImperial` depending on the `sex` and  `selectedSex` values.
            This is an overloaded method for just imperial cases.

        - Parameter dataImperial:   The data set where to extract from.
        - Parameter sex: The SexValue in String.
        - Parameter selectedSex: Optional parameter, used when sex is Human or Unisex.

        - Returns: The proper `SubjectDataImperial` data if the sex filters matches, `nil`otherwise.
    */
    static func getSubjectDataBySex(dataImperial: DataImperial, sex: String, selectedSex: String?) -> SubjectDataImperial? {
        var subjectData: SubjectDataImperial = SubjectDataImperial()
        
        switch sex {
        case SexValues.BABY.rawValue:
            subjectData = dataImperial.baby!
        case SexValues.CHILD.rawValue:
            subjectData = dataImperial.child!
        case SexValues.BOY.rawValue:
            subjectData = dataImperial.boy!
        case SexValues.GIRL.rawValue:
            subjectData = dataImperial.girl!
        case SexValues.FEMALE.rawValue:
            subjectData = dataImperial.female!
        case SexValues.MALE.rawValue:
            subjectData = dataImperial.male!
        case SexValues.HUMAN.rawValue:
            if   selectedSex != nil {
                subjectData = getSexForHumanType(selectedSex: selectedSex!, dataImperial: dataImperial)
            }
        case SexValues.UNISEX.rawValue:
            if   selectedSex != nil {
                subjectData = getSexForUnisexType(selectedSex: selectedSex!, dataImperial: dataImperial)
            }
        default:
            return nil
        }
        
        return subjectData
    }
    
    static func getSexForHumanType(selectedSex: String, dataImperial: DataImperial) -> SubjectDataImperial {
        if selectedSex == SexValues.BOY.rawValue {
            return dataImperial.boy!
        }
        return dataImperial.male!
    }
    
    static func getSexForUnisexType(selectedSex: String, dataImperial: DataImperial) -> SubjectDataImperial {
        if selectedSex == SexValues.FEMALE.rawValue {
            return dataImperial.female!
        }
        return dataImperial.male!
    }
}
