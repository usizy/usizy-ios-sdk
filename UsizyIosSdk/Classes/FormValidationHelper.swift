//
//  FormValidationHelper.swift
//  Pods
//
//  Created by David Arcis on 5/3/21.
//

import Foundation
import UIKit

class FormValidationHelper {
    static func areInputsValid() {
        // TODO -> implement teh validation depending on the shape of the forms
        fatalError("areInputsValid() has not been implemented")
    }
    
    static func someHasError(labels: UILabel...) -> Bool {
        var hasError: Bool = false
        
        for label in labels {
            if label.text != "" {
                hasError = true
            }
        }
        
        return hasError
    }
    
    static func isOutOfValidRange(value: String?, min: Float, max: Float) -> Bool {
        var isOut: Bool = false
        let floatValue: Float? = Float(value ?? "0")
        if (floatValue ?? 0 < min || floatValue ?? 0 > max ) {
            isOut = true
        }
        return isOut
    }
    
    static func setInputError(text: String, measureLimit: MeasureLimitMetric, errorLabel: UILabel, value: inout Any) {
        var min: Float
        var max: Float
        min = measureLimit.min!
        max = measureLimit.max!
        
        if isOutOfValidRange(value: text, min: min, max: max) {
            errorLabel.text = "\(Int(min)) - \(Int(max))"
        } else {
            errorLabel.text = ""
            value = Float(text) ?? 0
        }
    }
}
