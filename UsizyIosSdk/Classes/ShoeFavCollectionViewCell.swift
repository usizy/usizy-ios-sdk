//
//  ShoeFavCollectionViewCell.swift
//  UsizyIosSdk
//
//  Created by Daniel Almería Núñez on 14/4/21.
//

import UIKit

class ShoeFavCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet public weak var ivThumb: UIImageView!
    @IBOutlet public weak var lbText: UILabel!
}
