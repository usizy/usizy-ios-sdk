//
//  ResultsViewController.swift
//  Pods
//
//  Created by Daniel Almería Núñez on 03/03/2021.
//

import UIKit

class ResultsViewController: UIViewController {

    // MARK: - Public vars
    var contentView: ContentViewController?
    var popupData: OpenPopupResponse?
    var request: SaveRecommendRequest?
    var saveRecommendResponse: SaveRecommendResponse?
    var product: String?
    var keys: [String] = []
    var values: [String] = []
    var selectedSystem: Int = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var lbRecommendTitle: UILabel!
    @IBOutlet weak var aiRecommended: UIActivityIndicatorView!
    @IBOutlet weak var lbRecommendSizeInfo: UILabel!
    @IBOutlet weak var lbRecommendExtra: UILabel!
    @IBOutlet weak var vwSegmentedContainer: UIView!
    @IBOutlet weak var vwFooter: UIView!
    @IBOutlet weak var btnRecommendStart: UIButton!
    @IBOutlet weak var btnRecommendFinish: UIButton!
    
    // MARK: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()

        localized()
        fetchRecommend()
    }

    // MARK: - Internal
    private func localized(){
        lbRecommendTitle.text = localizedStringBundle(key: "title.results", for: self.classForCoder)
        lbRecommendSizeInfo.text = ""
        lbRecommendExtra.text = localizedStringBundle(key: "description.last", for: self.classForCoder)
        btnRecommendStart.setTitle(localizedStringBundle(key: "buttons.redo", for: self.classForCoder).uppercased(), for: .normal)
        btnRecommendFinish.setTitle(localizedStringBundle(key: "buttons.finalize", for: self.classForCoder).uppercased(), for: .normal)
    }
    
    private func fetchRecommend() {
        vwContainer.isHidden = true
        vwFooter.isHidden = true
        aiRecommended.startAnimating()
        
        SizeQuery().saveRecommend(self.request!) { (response: SaveRecommendResponse?) in
            self.aiRecommended.stopAnimating()
            
            if let validResponse = response{
                if (validResponse.usizy_size_result == SizeResult.MATCH.rawValue){
                    self.saveRecommendResponse = response
                    self.showRecommended(validResponse)
                } else {
                    self.showError(sizeResult: validResponse.usizy_size_result)
                }
            }else{
                self.showError(sizeResult: nil)
            }
        }
    }
    
    private func showError(sizeResult: String?){
        let storyboard = loadStoryboardBundle(for: self.classForCoder)
        let errorForm = storyboard.instantiateViewController(withIdentifier: "ErrorViewController") as! ErrorViewController
        errorForm.contentView = self.contentView
        errorForm.popupData = self.popupData
        errorForm.product = self.product
        if (sizeResult == SizeResult.OVERFLOW.rawValue) {
            errorForm.overflowMessage = localizedStringBundle(key: "OVERFLOW", for: self.classForCoder)
        } else if (sizeResult == SizeResult.UNDERFLOW.rawValue) {
            errorForm.overflowMessage = localizedStringBundle(key: "UNDERFLOW", for: self.classForCoder)
        }
        errorForm.shoes = request?.footType != nil
        self.contentView!.show(from: self, to: errorForm)
    }
    
    private func showRecommended(_ response: SaveRecommendResponse){
        vwContainer.isHidden = false
        vwFooter.isHidden = false
        
        self.lbRecommendSizeInfo.text = response.usizy_size_country
        
        if let validExtra = response.extra {
            
            for (index, (key, value)) in validExtra.enumerated() {
                if response.usizy_size_country_system == key {
                    selectedSystem = index
                }
                keys.append(key)
                values.append(value)
            }
            
            if (keys.count > 1){
                addSegmentedControl()
            }
        }
    }
    
    private func addSegmentedControl(){
        let customSC = UISegmentedControl(items: keys)
        customSC.selectedSegmentIndex = selectedSystem
        customSC.frame = CGRect(x: 0, y: 0, width: self.vwSegmentedContainer.frame.width, height: self.vwSegmentedContainer.frame.height)
        customSC.layer.cornerRadius = 4.0
        customSC.backgroundColor = UIColor.white
        customSC.tintColor = UIColor.colorPrimary
        
        if #available(iOS 13.0, *) {
            customSC.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            customSC.selectedSegmentTintColor = UIColor.colorPrimary
        }
        
        customSC.addTarget(self,  action: #selector(changeOption(sender:)), for: .valueChanged)
        self.vwSegmentedContainer.addSubview(customSC)
    }
    
    // MARK: - IBAction
    @IBAction func gotoStart(_ sender: Any) {
        if (request?.footType != nil){
            let storyboard = loadStoryboardBundle(for: self.classForCoder)
            let vc = storyboard.instantiateViewController(withIdentifier: "ShoesFormViewController") as! ShoesFormViewController
            vc.contentView = contentView
            vc.popupData = popupData
            vc.product = self.product
            contentView!.show(from: self, to: vc)
        }else{
            let storyboard = loadStoryboardBundle(for: self.classForCoder)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainFormViewController") as! MainFormViewController
            vc.contentView = contentView
            vc.popupData = popupData
            vc.product = self.product
            contentView!.show(from: self, to: vc)
        }
    }
    
    @IBAction func finish(_ sender: Any) {
        let userInfo:[AnyHashable : Any] = [
            "response": self.saveRecommendResponse!,
            "product": self.product!
        ]
        NotificationCenter.default.post(name: Notification.Name("didReceiveRecommendedSize"), object: nil, userInfo: userInfo)
        SharedBetweenFormsData.shared.clearValues()
        self.contentView?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - #selector
    @objc func changeOption(sender: UISegmentedControl) {
        self.lbRecommendSizeInfo.text = values[sender.selectedSegmentIndex]
    }
}
