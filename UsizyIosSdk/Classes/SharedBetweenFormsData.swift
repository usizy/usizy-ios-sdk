//
//  SharedBetweenFormsData.swift
//  Pods
//
//  Created by Cloud District on 16/3/21.
//

import Foundation

class SharedBetweenFormsData {
    static let shared = SharedBetweenFormsData()
    
    var selectedSex: String?
    var isFromAdvanced: Bool
    var isImperial: Bool
    
    var profileMale: ProfileMale
    var profileFemale: ProfileFemale
    var profileBoy: ProfileBoy
    var profileStandar: ProfileStandar
    
    private init() {
        isImperial = false
        isFromAdvanced = false
        selectedSex = nil
        
        profileBoy = ProfileBoy()
        profileMale = ProfileMale()
        profileFemale = ProfileFemale()
        profileStandar = ProfileStandar()
    }
    
    func clearValues() {
        isImperial = false
        isFromAdvanced = false
        selectedSex = nil
        
        profileBoy = ProfileBoy()
        profileMale = ProfileMale()
        profileFemale = ProfileFemale()
        profileStandar = ProfileStandar()
    }
    
    struct Measure {
        var metric: Float? = nil
        var imperial: Float? = nil
    }
    
    
    
    func getProfile(for sex: String?) -> Profile {
        var profile: Profile
        switch sex {
            case SexValues.UNISEX.rawValue:
                if selectedSex == SexValues.MALE.rawValue {
                    profile = self.profileMale
                } else {
                    profile = self.profileFemale
                }
            case SexValues.HUMAN.rawValue:
                if selectedSex == SexValues.MALE.rawValue {
                    profile = self.profileMale
                } else {
                    profile = self.profileBoy
                }
            default:
                profile = self.profileStandar
        }
        return profile
    }
    
}

class Profile {
    var metricHeight: Float? = nil
    var imperialHeight: HeightImperial? = nil
    var weight: SharedBetweenFormsData.Measure = SharedBetweenFormsData.Measure()
    var chest: SharedBetweenFormsData.Measure = SharedBetweenFormsData.Measure()
    var waist: SharedBetweenFormsData.Measure = SharedBetweenFormsData.Measure()
    var hips: SharedBetweenFormsData.Measure = SharedBetweenFormsData.Measure()
    var fit: Int? = nil
    var age: Int? = nil
    var chest_type: Int? = nil
    var waist_type: Int? = nil
    var hips_type: Int? = nil
}

class ProfileStandar : Profile {
    var type: String = "STD"
}

class ProfileMale : Profile {
    var type: String = SexValues.MALE.rawValue
}

class ProfileFemale : Profile {
    var type: String = SexValues.FEMALE.rawValue
}

class ProfileBoy : Profile {
    var type: String = SexValues.BOY.rawValue
}

