#
# Be sure to run `pod lib lint UsizyIosSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UsizyIosSdk'
  s.version          = '0.11.1'
  s.summary          = 'Usizy Size button SDK for iOS.'
  
  s.swift_version    = '5.0'
  
  s.ios.deployment_target = '13.4'
  
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
uSizy Size Adviser
Recommend the exact size to online shoppers for apparel and footwear, boosting sales, reducing returns and costs, and increasing customer loyalty
                       DESC

  s.homepage         = 'https://bitbucket.org/usizy/usizy-ios-sdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '11569480' => 'daniel.almeria@clouddistrict.com' }
  s.source           = { :git => 'https://bitbucket.org/usizy/usizy-ios-sdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.source_files = 'UsizyIosSdk/Classes/**/*'
  
   s.resource_bundles = {
     'UsizyIosSdk' => [
     'UsizyIosSdk/Assets/*.lproj/*.strings',
     'UsizyIosSdk/Assets/*.xcassets',
     'UsizyIosSdk/Classes/*.storyboard'
     ],
     'UsizyIosSdk.privacy' => 'PrivacyInfo.xcprivacy'
   }
   
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  
end
